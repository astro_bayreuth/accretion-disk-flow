/*	The following program code can be used to simulate turbulent flows in accretion disks. It is a finite difference code on a eulerian grid. The governing system of equations has been nondimensionalized
 * 	and physcially consist of
 *  
 * 	(1) a continuity equation for the surface density (because the accretion disk is modeled as a 2-dimensional system), 
 * 	(2) the Euler equations (in two dimensions) to determine the flow field based on momentum conservation, 
 * 	(3) an equation of state to link the pressure with the internal energy (or vice versa), 
 * 	(4) a transport equation for the internal energy to capture shocks and cooling,
 * 	(5) a poisson equation to determine the gravitational potential out of the surface density.
 * 
 * 	Numerically, only 5 quantities are required (the nondimensionalized pressure is equal to the nondimensionalized internal energy density):
 * 
 * 	1. the gravitational potential
 * 	2. the surface density
 * 	3. the internal energy density 
 * 	4. the (scalar-valued) velocity field in radial (x) direction
 * 	5. the (scalar-valued) velocity field in poloidal (y) direction
 * 
 * 	The grid consists of N_radial * N_poloidal grid points accessed by (i,j). A third index (k) is used to adress a respective quantity (k = 1: gravitational potential, k = 2: surface density, ...).
 * 	To avoid unnecessary multidimensional arrays (as usual in C), a single pseudo-multidimensional vector combining_vector is used that summarizes all 5 quantities for each grid point, i. e. the 
 * 	vector contains 5 * N_radial * N_poloidal entries. Another reason to use a single, pseudo-multidimensional vector is that the fast fourier transform that solves the poisson equation to determine the
 * 	gravitational potential out of the surface density (using the FFTW3 library) only takes those one-dimensional arrays (vectors) if the memory is allocated dynamically. So unnecessary and time-consuming
 * 	value-transfer can be avoided. Input parameters are changed by a JSON script in a way that one doesn't need to compile the code after each change of a parameter but just have to make a new run.
 * 
 *	The derivatives are numerically realised by central differences up to the 8th order, for time-integration it can be chosen between euler scheme or a Runge Kutta scheme of 3rd as well as 4th order. At the
 * 	moment (09.03.18), only linear interpolation between two grid points is used.
 * 
 * 	Explanations (i. e. comments) to individual program sections will be given in capital letters before a section begins. On the contrary, comments on individual program lines are made in the same line.
 *
 * 	Compile program (example): g++ -std=c++11 -lfftw3 -lm -I /home/btpp/btp00000/shared/cJSON -L /home/btpp/btp00000/shared/cJSON/lib -lcjson disk_flow.cpp -o run
 * 	Run program (if compiled in the way shown above): ./run
 * 	Hint: The command must include an instruction for the compiler to link the program with the FFTW3 library (-lfftw3 -lm) as well as the cJSON library. 
 * 
 *
 * 
 *	################################################################################################################################################################################################################
 */



// HEADER (INCLUDE ALL REQUIRED LIBRARIES)



#include <iostream>																		// Include standard input-/output-stream
#include <vector>																		// Include vector-class (replaces classical C-arrays in C++11)
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdio.h>																		// printf, scanf, puts, NULL 
#include <stdlib.h>    																		// srand, rand, abs 
#include <time.h>		
#include <include/cjson/cJSON.h> 																// home/btpp/btp00000/shared/source_me_to_have_them_all.sh has to be added to .bashrc
#include <stdbool.h>
#include <unistd.h>
#include <fftw3.h>																		// Include FFTW3 library for the fast fourier transform
#include <iomanip>
#include <limits>
#include <ctime>
#include <chrono>
#include <cmath>
#include <omp.h>



using namespace std;
using namespace std::chrono;



// DEFINING MACROS (CONSTANTS)



#define PI 3.14159265358979323846264338327950
#define REAL 0
#define IMAG 1
#define DEFAULT_INPUT_FILENAME "input_diskFlow.json"



// DECLARING GLOBAL VARIABLES (INPUT VIA JSON)



int n_radial;																			// Number of grid points in radial (x) direction
int n_poloidal;																			// Number of grid points in poloidal (y) direction
int n_ghost;																			// Number of ghost zones
int integration_method;																		// Integration method: '1' (Euler), '4' (Runge Kutta 4th order)
int derivative_method; 																		// Order of central difference scheme: '1' (2nd order), '2' (4th order), '3' (6th order), '4' (8th order)
int n_output;																			// Data output after n_output time steps
int initialization;																		// Chooses the kind of initialization of Phi, Sigma, U, u and v for t = 0.0
int frac;																			// Stes upper limit for noise-frequency
int restart_interval;
double time_limit;																		// Time limit, until this point in time the simulation runs
double time_step;																		// Time step, this is the time step in the integration scheme
double box_length_radial;																	// Nondimensionalized box length in radial (x) direction
double box_length_poloidal;																	// Nondimensionalized box length in poloidal (y) direction
double q_toomre;																		// Dimensionless toomre parameter (stability criterion)
double cs_0;																			// Speed of sound at initialisation-time
double kepler_prefactor;																	// Exponent of keplerian potential, 3/2 
double cooling_time;																		// Ratio of time to characteristic cooling time
double cooling_mode;																		// Decides whether the cooling floor is homogenous and constant or local and density dependent
double adiabatic_index;																		// Adiabatic index
double equilibrium_density;																	// Equilibrium value of the density, depends on the Toomre parameter Q and the adiabatic index
double U_eq;																			// Internal energy density related to "q_toomre_eq" corresponding to the cooling-equilibrium (that values has nothing to do with the initialisation and only serves as a cooling treshold)
double sigma_amp;																		// Amplitude of the initial density perturbation
double internal_amp;																		// Amplitude of the initial internal energy density perturbation
double ux_amp;																			// Amplitude of the initial radial velocity perturbation
double uy_amp;																			// Amplitude of the initial poloidal velocity perturbation
double scaling;																			// Allows for scaling of the initial amplitude in case the run is a restart
double time_restart;																		// Time at which 'Restart' takes place
double rey_stress;																		// xy-component of the reynolds stress tensor with turbulent vy
double rey_stress_0;																		// xy-component of the reynolds stress tensor with background vy0
double rey_vel_only;																		// Spatial average of vx*delta_vy
double mean_U;																			// Mean internal energy, neccessary in order to normalize the stresses
double alpha_norm;																		// Normalization factor to get alpha out of the turbulent stress tensor
double q_cumulated;																		// Dummy variable for calculation of the averaged toomre-Q
double sonic_cumulated;																		// Dummy variable for calculation of the averaged sound speed cs

double switch_compressible;																	// Declare switch for compressibility terms div u
double switch_density_grad;																	// Declare switch for terms including density gradients grad SIGMA
double switch_pressure_grad;																	// Declare switch for terms including pressure gradients grad p
double switch_coriolis;																		// Declare switch for terms corresponding to coriolis force
double switch_centrifugal;																	// Declare switch for terms corresponding to centrifugal force
double switch_advective;																	// Declare switch for advective terms
double switch_cooling;																		// Declare switch for cooling function
double switch_energy_grad;																	// Declare switch for terms including energy gradients grad U
double switch_gravi_grad;																	// Declare switch for terms including gravitational potential gradients grad PHI

double damping_coeff_viscous_pressure;																// Declare damping coefficient for the artificial viscous pressure


double damping_coeff_2_density_radial;																// Declare damping coefficient (switch) for 2nd order artificial viscosity for surface density in radial (x) direction
double damping_coeff_2_density_poloidal;															// Declare damping coefficient (switch) for 2nd order artificial viscosity for surface density in poloidal (y) direction
double damping_coeff_2_energy_radial;																// Declare damping coefficient (switch) for 2nd order artificial viscosity for internal energy in radial (x) direction
double damping_coeff_2_energy_poloidal;																// Declare damping coefficient (switch) for 2nd order artificial viscosity for internal energy in poloidal (y) direction
double damping_coeff_2_xvelocity_radial;															// Declare damping coefficient (switch) for 2nd order artificial viscosity for radial (x) velocity in radial (x) direction
double damping_coeff_2_xvelocity_poloidal;															// Declare damping coefficient (switch) for 2nd order artificial viscosity for radial (x) velocity in poloidal (y) direction
double damping_coeff_2_yvelocity_radial;															// Declare damping coefficient (switch) for 2nd order artificial viscosity for poloidal (y) velocity in radial (x) direction
double damping_coeff_2_yvelocity_poloidal;															// Declare damping coefficient (switch) for 2nd order artificial viscosity for polodial (y) velocity in poloidal (y) direction
double d_max;
double damp_flip_point;
double damp_sharpness;
double start;																			// Declare system time variable for performance tests
double end;																			// Declare system time variable for performance tests
double total_time;																		// Declare system time variable for performance tests
double divergence = 0.0;
double thermal_grad_x = 0.0;
double thermal_grad_y = 0.0;
double delta_out;
double time_step_old;
double vx_max;
double vy_max;
double cfl;
double nghost;

bool switch_viscous_pressure;																	// Declare boolean variable / switch to affect whether the artificial viscous pressure term is considered or not
bool mean_kinetic_energy;																	// Declare boolean variable to affect whether a file containing the averaged kinetic energy for the whole computational domain is written out after each n_output time steps
bool mean_thermal_energy;																	// Declare boolean variable to affect whether a file containing the averaged thermal energy for the whole computational domain is written out after each n_output time steps
bool mean_gravitational_energy;																	// Declare boolean variable to affect whether a file containing the averaged gravitational energy for the whole computational domain is written out after each n_output time steps
bool mean_squared_surface_density;																// Declare boolean variable to affect whether a file containing the squared averaged surface density for the whole computational domain is written out after each n_output time steps
bool mass_conservation_check;																	// Declare boolean variable to affect whether a file containing the normalized surface density perturbation on the computational domain is written out to check if the mass is conserved
bool xy_gravitational_potential;																// Declare boolean variable to affect whether a file containing the gravitational potential for each grid point is written out after each n_output time steps 
bool xy_surface_density;																	// Declare boolean variable to affect whether a file containing the surface density for each grid point is written out after each n_output time steps 
bool xy_internal_energy;																	// Declare boolean variable to affect whether a file containing the internal energy for each grid point is written out after each n_output time steps 
bool xy_radial_velocity;																	// Declare boolean variable to affect whether a file containing the radial velocity for each grid point is written out after each n_output time steps 
bool xy_poloidal_velocity;																	// Declare boolean variable to affect whether a file containing the poloidal velocity for each grid point is written out after each n_output time steps 
bool x_poloidal_velocity;																	// Declare boolean variable to affect whether a file containing the radial (poloidal-averaged) velocity is written out after each n_output time steps 
bool input_file;
bool grid_file;
bool stress;
bool sync_UQ;
bool spectrum;
bool mean_Q;
bool damping_check;
bool save_ghost;

bool gnuplot_format;																		// Switch to set whether the output files are produced in a format appropriate for gnuplot
bool general_format;																		// Switch to set whether the output files are produced in a format appropriate for any plot program



// DECLARE FUNCTIONS (USING FORWARD DECLARATON, SEE BELOW WHERE THEY ARE DEFINED FOR FURTHER EXPLANATIONS)



int c_index_function(int i, int j, int k, int n_pol);
int ghost_index(int i, int j, int k, int n_pol, int n_ghost);
int k_index_function(int i, int j, int k, int l, int n_pol, int n_rad);
double poloidal_1st_derivative_artificial_viscous_pressure(vector<double> & vis_press, vector<double> const & prefac_list_1, int i, int j, int n_pol, double delta_pol, int deri_meth);
double radial_1st_derivative_artificial_viscous_pressure(vector<double> & vis_press, vector<double> const & prefac_list_1, int i, int j, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing);
double poloidal_1st_derivative(vector<double> & com_vec, vector<double> const & prefac_list_1, int i, int j, int k, int n_pol, double delta_pol, int deri_meth);
double radial_1st_derivative(vector<double> & com_vec, vector<double> const & prefac_list_1, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost);
double poloidal_2nd_derivative(vector<double> & com_vec, vector<double> const & prefac_list_2, int i, int j, int k, int n_pol, double delta_pol, int deri_meth);
double radial_2nd_derivative(vector<double> & com_vec, vector<double> const & prefac_list_2, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost);
double poloidal_4th_derivative(vector<double> & com_vec, vector<double> const & prefac_list_4, int i, int j, int k, int n_pol, double delta_pol, int deri_meth);
double radial_4th_derivative(vector<double> & com_vec, vector<double> const & prefac_list_4, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost);
void k_vector_components_of_grid(vector<double> & k_rad, vector<double> & k_pol, vector<double> & k_matrix, int n_rad, int n_pol, double box_len_rad, double box_len_pol, bool grid_file);
void fft_solve_poisson_equation(fftw_plan p_forward_transform, fftw_plan p_backward_transform, fftw_complex * in_array, fftw_complex * out_array_forward, fftw_complex * out_array_backward, vector<double> & com_vec, vector<double> & k_matrix, vector<double> & k_rad, vector<double> & k_pol, double equi_density, int n_rad, int n_pol, double time, double delta_rad, double delta_pol, double box_len_rad, double box_len_pol, int n_out, double norm, bool spectr);
void rhs_function(vector<double> & com_vec, vector<double> & vis_press, vector<double> & vis_press_grad, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double v_boost, int l, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press);
void euler_scheme(vector<double> & com_vec, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press);
void runge_kutta_3rd_order(vector<double> & com_vec, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press);
void runge_kutta_4th_order(vector<double> & com_vec, vector<double> & ghost_vector, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, int n_ghost, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press, double current_time);
void value_check_function(vector<double> & com_vec, int n_rad, int n_pol);
void fill_the_ghost(vector<double> & ghost_vec, int n_rad, int n_pol, int n_ghost, double delta_rad, double delta_pol, double box_len_rad, double box_len_pol, double v_boost, double shearing);

void write_wave_amplitude(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol);
void write_mass_conservation_check(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol);
void write_averaged_kinetic_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, double delta_rad, int n_rad, int n_pol);
void write_averaged_internal_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol);
void write_averaged_gravitational_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol);
void write_averaged_squared_modulus(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol, int k);
void write_xy_data_gnu(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k, double box_len_rad, double box_len_pol);
void write_xy_data_gen(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k, double box_len_rad, double box_len_pol);
void write_ghost_data_gen(string const filename, vector<double> & ghost_vec, int n_rad, int n_pol, int n_ghost, int k, double box_len_rad, double box_len_pol);
void write_x_data_averaged_over_y(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k);



// JSON PARSING

void parseInputFile(const char *filename)
{
    char *buffer = 0;
    long length;
    FILE *f = fopen(filename, "rb");
    
    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell (f);
        fseek(f, 0, SEEK_SET);
        buffer = new char[length]; 
        
        if(buffer)
        {
            fread(buffer, 1, length, f);
        }
        fclose(f);   
                
    } else {
        printf("\nFILE '%s' COULD NOT BE READ.\n", filename);
        exit(1);
    }
    printf("\nFILE '%s' SUCCESSFULLY READ.\n\n", filename);
    
    if(buffer)
    {	
        cJSON *root = cJSON_Parse(buffer);
               
        cJSON *group;
             
        cJSON *parameterValue; 
        
        
        // GRID PARAMETER
	
        group = cJSON_GetObjectItem(root, "Grid parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "N_radial");
		if(cJSON_IsNumber(parameterValue)){n_radial = parameterValue -> valueint;}
		else{n_radial = 64;}
		
		parameterValue = cJSON_GetObjectItem(group, "N_poloidal");
		if(cJSON_IsNumber(parameterValue)){n_poloidal = parameterValue -> valueint;}
		else{n_poloidal = 64;}
		
		parameterValue = cJSON_GetObjectItem(group, "N_ghost");
		if(cJSON_IsNumber(parameterValue)){n_ghost = parameterValue -> valueint;}
		else{n_ghost = 2;}
		
		parameterValue = cJSON_GetObjectItem(group, "Box_length_radial");
		if(cJSON_IsNumber(parameterValue)){box_length_radial = parameterValue -> valuedouble;}
		else{box_length_radial = 30.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Box_length_poloidal");
		if(cJSON_IsNumber(parameterValue)){box_length_poloidal = parameterValue -> valuedouble;}
		else{box_length_poloidal = 30.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Initialization");
		if(cJSON_IsNumber(parameterValue)){initialization = parameterValue -> valueint;}
		else{initialization = 1;}
		
	// NUMERICAL PARAMETER	
	
	group = cJSON_GetObjectItem(root, "Numerical parameter");
	
		parameterValue = cJSON_GetObjectItem(group, "Time_step");
		if(cJSON_IsNumber(parameterValue)){time_step = parameterValue -> valuedouble;}
		else{time_step = 0.001;}
		
		parameterValue = cJSON_GetObjectItem(group, "Time_limit");
		if(cJSON_IsNumber(parameterValue)){time_limit = parameterValue -> valuedouble;}
		else{time_limit = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Courant_number");
		if(cJSON_IsNumber(parameterValue)){cfl = parameterValue -> valuedouble;}
		else{cfl = 0.8;}
		
		parameterValue = cJSON_GetObjectItem(group, "Integration_method");
		if(cJSON_IsNumber(parameterValue)){integration_method = parameterValue -> valueint;}
		else{integration_method = 4;}
		
		parameterValue = cJSON_GetObjectItem(group, "Derivative_method");
		if(cJSON_IsNumber(parameterValue)){derivative_method = parameterValue -> valueint;}
		else{derivative_method = 1;}
		
		parameterValue = cJSON_GetObjectItem(group, "Time_restart");
		if(cJSON_IsNumber(parameterValue)){time_restart = parameterValue -> valuedouble;}
		else{time_restart = 0.0;}
		
	// PHYSICAL PARAMETER
	
	group = cJSON_GetObjectItem(root, "Physical parameter");
	
		parameterValue = cJSON_GetObjectItem(group, "Q_toomre");
		if(cJSON_IsNumber(parameterValue)){q_toomre = parameterValue -> valuedouble;}
		else{q_toomre = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Sound_Speed");
		if(cJSON_IsNumber(parameterValue)){cs_0 = parameterValue -> valuedouble;}
		else{cs_0 = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Kepler_prefactor");
		if(cJSON_IsNumber(parameterValue)){kepler_prefactor = parameterValue -> valuedouble;}
		else{kepler_prefactor = 1.5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Cooling_time");
		if(cJSON_IsNumber(parameterValue)){cooling_time = parameterValue -> valuedouble;}
		else{cooling_time = 10.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Cooling_mode");
		if(cJSON_IsNumber(parameterValue)){cooling_mode = parameterValue -> valuedouble;}
		else{cooling_mode = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Adiabatic_index");
		if(cJSON_IsNumber(parameterValue)){adiabatic_index = parameterValue -> valuedouble;}
		else{adiabatic_index = 2.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "U_equilibrium");
		if(cJSON_IsNumber(parameterValue)){U_eq = parameterValue -> valuedouble;}
		else{U_eq = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Sync_UQ");
		if(cJSON_IsBool(parameterValue)){sync_UQ = cJSON_IsTrue(parameterValue);}
		else{sync_UQ = false;}
		
	// INITIAL PERTURBATION
	
	group = cJSON_GetObjectItem(root, "Initial perturbation");
	
		parameterValue = cJSON_GetObjectItem(group, "Sigma_amp");
		if(cJSON_IsNumber(parameterValue)){sigma_amp = parameterValue -> valuedouble;}
		else{sigma_amp = 1e-4;}
		
		parameterValue = cJSON_GetObjectItem(group, "Internal_amp");
		if(cJSON_IsNumber(parameterValue)){internal_amp = parameterValue -> valuedouble;}
		else{internal_amp = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Ux_amp");
		if(cJSON_IsNumber(parameterValue)){ux_amp = parameterValue -> valuedouble;}
		else{ux_amp = 1e-3;}
		
		parameterValue = cJSON_GetObjectItem(group, "Uy_amp");
		if(cJSON_IsNumber(parameterValue)){uy_amp = parameterValue -> valuedouble;}
		else{uy_amp = 1e-3;}
		
		parameterValue = cJSON_GetObjectItem(group, "Frac");
		if(cJSON_IsNumber(parameterValue)){frac = parameterValue -> valuedouble;}
		else{frac = 4.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Scaling");
		if(cJSON_IsNumber(parameterValue)){scaling = parameterValue -> valuedouble;}
		else{scaling = 1.0;}
		
	// SWITCHES
	
	group = cJSON_GetObjectItem(root, "Switches");
	
		parameterValue = cJSON_GetObjectItem(group, "Switch_compressible");
		if(cJSON_IsNumber(parameterValue)){switch_compressible = parameterValue -> valuedouble;}
		else{switch_compressible = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_density_grad");
		if(cJSON_IsNumber(parameterValue)){switch_density_grad = parameterValue -> valuedouble;}
		else{switch_density_grad = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_pressure_grad");
		if(cJSON_IsNumber(parameterValue)){switch_pressure_grad = parameterValue -> valuedouble;}
		else{switch_pressure_grad = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_coriolis");
		if(cJSON_IsNumber(parameterValue)){switch_coriolis = parameterValue -> valuedouble;}
		else{switch_coriolis = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_centrifugal");
		if(cJSON_IsNumber(parameterValue)){switch_centrifugal = parameterValue -> valuedouble;}
		else{switch_centrifugal = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_advective");
		if(cJSON_IsNumber(parameterValue)){switch_advective = parameterValue -> valuedouble;}
		else{switch_advective = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_cooling");
		if(cJSON_IsNumber(parameterValue)){switch_cooling = parameterValue -> valuedouble;}
		else{switch_cooling = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_energy_grad");
		if(cJSON_IsNumber(parameterValue)){switch_energy_grad = parameterValue -> valuedouble;}
		else{switch_energy_grad = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_gravi_grad");
		if(cJSON_IsNumber(parameterValue)){switch_gravi_grad = parameterValue -> valuedouble;}
		else{switch_gravi_grad = 1.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Switch_viscous_pressure");
		if(cJSON_IsBool(parameterValue)){switch_viscous_pressure = cJSON_IsTrue(parameterValue);}
		else{switch_viscous_pressure = true;}
	
	// ARTIFICIAL VISCOSITY
	
	group = cJSON_GetObjectItem(root, "Artificial viscosity");
	
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_viscous_pressure");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_viscous_pressure = parameterValue -> valuedouble;}
		else{damping_coeff_viscous_pressure = 0.0;}
	
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_density_radial");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_density_radial = parameterValue -> valuedouble;}
		else{damping_coeff_2_density_radial = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_density_poloidal");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_density_poloidal = parameterValue -> valuedouble;}
		else{damping_coeff_2_density_poloidal = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_energy_radial");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_energy_radial = parameterValue -> valuedouble;}
		else{damping_coeff_2_energy_radial = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_energy_poloidal");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_energy_poloidal = parameterValue -> valuedouble;}
		else{damping_coeff_2_energy_poloidal = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_xvelocity_radial");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_xvelocity_radial = parameterValue -> valuedouble;}
		else{damping_coeff_2_xvelocity_poloidal = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_xvelocity_poloidal");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_xvelocity_poloidal = parameterValue -> valuedouble;}
		else{damping_coeff_2_xvelocity_poloidal = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_yvelocity_radial");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_yvelocity_radial = parameterValue -> valuedouble;}
		else{damping_coeff_2_yvelocity_radial = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_2_yvelocity_poloidal");
		if(cJSON_IsNumber(parameterValue)){damping_coeff_2_yvelocity_poloidal = parameterValue -> valuedouble;}
		else{damping_coeff_2_yvelocity_poloidal = 0.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Maximum_damping");
		if(cJSON_IsNumber(parameterValue)){d_max = parameterValue -> valuedouble;}
		else{d_max = 0.5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_flip_point");
		if(cJSON_IsNumber(parameterValue)){damp_flip_point = parameterValue -> valuedouble;}
		else{damp_flip_point = 1.5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_sharpness");
		if(cJSON_IsNumber(parameterValue)){damp_sharpness = parameterValue -> valuedouble;}
		else{damp_sharpness = 2.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Damping_Evolution");
		if(cJSON_IsBool(parameterValue)){damping_check = cJSON_IsTrue(parameterValue);}
		else{damping_check = true;}
	
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_density_radial");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_density_radial = parameterValue -> valuedouble;}
		//else{damping_coeff_4_density_radial = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_density_poloidal");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_density_poloidal = parameterValue -> valuedouble;}
		//else{damping_coeff_4_density_poloidal = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_energy_radial");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_energy_radial = parameterValue -> valuedouble;}
		//else{damping_coeff_4_energy_radial = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_energy_poloidal");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_energy_poloidal = parameterValue -> valuedouble;}
		//else{damping_coeff_4_energy_poloidal = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_xvelocity_radial");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_xvelocity_radial = parameterValue -> valuedouble;}
		//else{damping_coeff_4_xvelocity_poloidal = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_xvelocity_poloidal");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_xvelocity_poloidal = parameterValue -> valuedouble;}
		//else{damping_coeff_4_xvelocity_poloidal = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_yvelocity_radial");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_yvelocity_radial = parameterValue -> valuedouble;}
		//else{damping_coeff_4_yvelocity_radial = 0.0;}
		
		//parameterValue = cJSON_GetObjectItem(group, "Damping_coeff_4_yvelocity_poloidal");
		//if(cJSON_IsNumber(parameterValue)){damping_coeff_4_yvelocity_poloidal = parameterValue -> valuedouble;}
		//else{damping_coeff_4_yvelocity_poloidal = 0.0;}
		
	// DIAGNOSTICS
	
	group = cJSON_GetObjectItem(root, "Diagnostics");
	
		parameterValue = cJSON_GetObjectItem(group, "N_output");
		if(cJSON_IsNumber(parameterValue)){n_output = parameterValue -> valueint;}
		else{n_output = 100;}
		
		parameterValue = cJSON_GetObjectItem(group,"Save_Restart_Interval");
		if(cJSON_IsNumber(parameterValue)){restart_interval = parameterValue -> valueint;}
		else{restart_interval = 20;}
		
		parameterValue = cJSON_GetObjectItem(group, "Delta_out");
		if(cJSON_IsNumber(parameterValue)){delta_out = parameterValue -> valuedouble;}
		else{delta_out = 1.0;}

		parameterValue = cJSON_GetObjectItem(group, "Input_file");
		if(cJSON_IsBool(parameterValue)){input_file = cJSON_IsTrue(parameterValue);}
		else{input_file = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Grid_file");
		if(cJSON_IsBool(parameterValue)){grid_file = cJSON_IsTrue(parameterValue);}
		else{grid_file = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Mean_kinetic_energy");
		if(cJSON_IsBool(parameterValue)){mean_kinetic_energy = cJSON_IsTrue(parameterValue);}
		else{mean_kinetic_energy = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Mean_thermal_energy");
		if(cJSON_IsBool(parameterValue)){mean_thermal_energy = cJSON_IsTrue(parameterValue);}
		else{mean_thermal_energy = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Mass_conservation_check");
		if(cJSON_IsBool(parameterValue)){mass_conservation_check = cJSON_IsTrue(parameterValue);}
		else{mass_conservation_check = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Mean_gravitational_energy");
		if(cJSON_IsBool(parameterValue)){mean_gravitational_energy = cJSON_IsTrue(parameterValue);}
		else{mean_gravitational_energy = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Mean_squared_surface_density");
		if(cJSON_IsBool(parameterValue)){mean_squared_surface_density = cJSON_IsTrue(parameterValue);}
		else{mean_squared_surface_density = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "XY_gravitational_potential");
		if(cJSON_IsBool(parameterValue)){xy_gravitational_potential = cJSON_IsTrue(parameterValue);}
		else{xy_gravitational_potential = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "XY_surface_density");
		if(cJSON_IsBool(parameterValue)){xy_surface_density = cJSON_IsTrue(parameterValue);}
		else{xy_surface_density = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "XY_internal_energy");
		if(cJSON_IsBool(parameterValue)){xy_internal_energy = cJSON_IsTrue(parameterValue);}
		else{xy_internal_energy = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "XY_radial_velocity");
		if(cJSON_IsBool(parameterValue)){xy_radial_velocity = cJSON_IsTrue(parameterValue);}
		else{xy_radial_velocity = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "XY_poloidal_velocity");
		if(cJSON_IsBool(parameterValue)){xy_poloidal_velocity = cJSON_IsTrue(parameterValue);}
		else{xy_poloidal_velocity = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "X_poloidal_velocity");
		if(cJSON_IsBool(parameterValue)){x_poloidal_velocity = cJSON_IsTrue(parameterValue);}
		else{x_poloidal_velocity = false;}	
		
		parameterValue = cJSON_GetObjectItem(group, "Stress");
		if(cJSON_IsBool(parameterValue)){stress = cJSON_IsTrue(parameterValue);}
		else{stress = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Spectrum");
		if(cJSON_IsBool(parameterValue)){spectrum = cJSON_IsTrue(parameterValue);}
		else{spectrum = true;}	
		
		parameterValue = cJSON_GetObjectItem(group, "Mean_Q");
		if(cJSON_IsBool(parameterValue)){mean_Q = cJSON_IsTrue(parameterValue);}
		else{mean_Q = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Save_ghost");
		if(cJSON_IsBool(parameterValue)){save_ghost = cJSON_IsTrue(parameterValue);}
		else{save_ghost = false;}
		
	// DIAGNOSTICS FORMAT
	
	group = cJSON_GetObjectItem(root, "Diagnostics format");
		
		parameterValue = cJSON_GetObjectItem(group, "Gnuplot_format");
		if(cJSON_IsBool(parameterValue)){gnuplot_format = cJSON_IsTrue(parameterValue);}
		else{gnuplot_format = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "General_format");
		if(cJSON_IsBool(parameterValue)){general_format = cJSON_IsTrue(parameterValue);}
		else{general_format = false;}
		
        cJSON_Delete(root);
        
        delete[] buffer;
    }
    
    return;
}



/* ############################
 * Starting with main function
 * ############################
 */

int main()
{
  // READ INPUT PARAMETER 
  

  parseInputFile(DEFAULT_INPUT_FILENAME);
  
  
  // SEED THE PSEUDO RANDOM VALUE GENERATOR
  
  
  srand(time(NULL));
  
  
  // DEFINE CONSTANT VALUES (DETERMINE OUT OF INPUT PARAMETER IN JSON FILE)
  
  
  const double delta_radial = box_length_radial/n_radial;															// Determine size of finite difference step in x-direction / radial direction
  const double delta_poloidal = box_length_poloidal/n_poloidal;															// Determine size of finite difference step in y-direction / poloidal direction
  const double v_boost_poloidal = kepler_prefactor * box_length_radial;														// Determine nondimensionalized boost velocity in poloidal (y) direction
  const double equilibrium_density = cs_0/( PI * q_toomre);															// Determine the equilibrium density value out of the input parameter Toomre Q and adiabatic index
  double phase[n_radial][n_poloidal];
  const double d_old = damping_coeff_2_density_radial;
  const double damp_range = d_max - d_old;
  
  
  
  // ONE USUALLY WANTS THE INITIAL STATE TO BE IN EQUILIBRIUM. THEREFORE, U AND Q WILL HERE BE SYNCHRONIZED IF THAT HAS BEEN SPECIFIED IN THE INPUT FILE.
  
  if(sync_UQ)
  {
      U_eq = (pow(cs_0, 3.0)/ (PI * q_toomre)) * 1.0/ (adiabatic_index * (adiabatic_index - 1.0));
  }
  
  
  
  
  // ALLOCATE ARRAYS (USING C++11 <VECTOR> CLASS, NO HEAP MEMORY MANAGEMENT REQUIRED, ESPECIALLY NO DELETE)
  
  
  vector<double> prefactor_list_1st_derivative {0.0, 0.0, 0.0, -(1.0/2.0), 0.0, (1.0/2.0), 0.0, 0.0, 0.0, 0.0, 0.0, (1.0/12.0), -(2.0/3.0), 0.0, (2.0/3.0), -(1.0/12.0), 0.0, 0.0, 0.0, -(1.0/60.0), (3.0/20.0), -(3.0/4.0), 0.0, (3.0/4.0), -(3.0/20.0), (1.0/60.0), 0.0, (1.0/280.0), -(4.0/105.0), (1.0/5.0), -(4.0/5.0), 0.0, (4.0/5.0), -(1.0/5.0), (4.0/105.0), -(1.0/280.0)}; // Contains the prefactors for the 1st derivatives for finite difference scheme	
  vector<double> prefactor_list_2nd_derivative {0.0, 0.0, 0.0, 1.0, -2.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, -(1.0/12.0), (4.0/3.0), -(5.0/2.0), (4.0/3.0), -(1.0/12.0), 0.0, 0.0, 0.0, (1.0/90.0), -(3.0/20.0), (3.0/2.0), -(49.0/18.0), (3.0/2.0), -(3.0/20.0), (1.0/90.0), 0.0, -(1.0/560.0), (8.0/315.0), -(1.0/5.0), (8.0/5.0), -(205.0/72.0), (8.0/5.0), -(1.0/5.0), (8.0/315.0), -(1.0/560.0)}; // Contains the prefactors for the 2nd derivatives for finite difference scheme
  vector<double> prefactor_list_4th_derivative {0.0, 0.0, 1.0, -4.0, 6.0, -4.0, 1.0, 0.0, 0.0, 0.0, -(1.0/6.0), 2.0, -(13.0/2.0), (28.0/3.0), -(13.0/2.0), 2.0, -(1.0/6.0), 0.0, (7.0/240.0), -(2.0/5.0), (169.0/60.0), -(122.0/15.0), (91.0/8.0), -(122.0/15.0), (169.0/60.0), -(2.0/5.0), (7.0/240.0)}; // Contains the prefactors for the 4th derivatives for finite difference scheme
  vector<double> combining_vector (5 * n_radial * n_poloidal);
  vector<double> ghost (5 * (n_radial + 2 * n_ghost) * (n_poloidal + 2 * n_ghost));
  vector<double> k_radial_vector (n_radial);
  vector<double> k_poloidal_vector (n_poloidal);
  vector<double> inverse_k_absolute_matrix (n_radial * n_poloidal);
  vector<double> k_store_array (integration_method * 4 * n_radial * n_poloidal);
  vector<double> viscous_pressure_array (n_radial * n_poloidal);
  
  
  // ALLOCATE ARRAYS (USING C ARRAYS AS REQUIRED FOR THE FFTW3 LIBRARY, HEAP MEMORY MANAGEMENT IS REQUIRED!!!)
  
  fftw_complex * in_array;
  fftw_complex * out_array_forward;
  fftw_complex * out_array_backward;
  in_array = (fftw_complex*) fftw_malloc(n_radial * n_poloidal * sizeof(fftw_complex));
  out_array_forward = (fftw_complex*) fftw_malloc(n_radial * n_poloidal * sizeof(fftw_complex));
  out_array_backward = (fftw_complex*) fftw_malloc(n_radial * n_poloidal * sizeof(fftw_complex));
  
  
  // CREATE A PLAN 'FORWARD TRANSFORMATION' (SEE FFTW3 MANUAL FOR FURTHER DESCRIPTION WHY THIS IS NECESSARY)
  
  
  fftw_plan p_forward_transform;
  p_forward_transform = fftw_plan_dft_2d(n_radial, n_poloidal, in_array, out_array_forward, FFTW_FORWARD, FFTW_ESTIMATE);
  
  
  // ... AND OF COURSE A PLAN 'BACKWARD TRANSFORMATION' FOR THE INVERSE FOURIER TRANSFORM AFTER SOLVING THE POISSON EQUATION
  
  
  fftw_plan p_backward_transform;
  p_backward_transform = fftw_plan_dft_2d(n_radial, n_poloidal, out_array_forward, out_array_backward, FFTW_BACKWARD, FFTW_ESTIMATE);
  

  
  // WRITE INPUT DATA IN FILE
  
  
  if(input_file)
  {
    stringstream input_data_filename;
    input_data_filename << "input_parameter_gen_" << time_restart << ".dat";
    fstream input_parameter;
    input_parameter.open (input_data_filename.str(), ios::out);
  
    input_parameter << "Grid parameter:" << endl;
    input_parameter << '\n';
    input_parameter << "N_radial:\t \t" << n_radial << endl;
    input_parameter << "N_poloidal:\t \t" << n_poloidal << endl;
    input_parameter << "N_ghost:\t \t" << n_ghost << endl;
    input_parameter << "Number of grid points:\t" << n_radial * n_poloidal << endl;
    input_parameter << "Box_length_radial:\t" << box_length_radial << endl;
    input_parameter << "Box_length_poloidal:\t" << box_length_poloidal << endl;
    input_parameter << "Initialization: \t" << initialization << endl;
    input_parameter << '\n';
    input_parameter << "Numerical parameter:" << endl;
    input_parameter << '\n';
    input_parameter << "Time_step:\t \t" << time_step << endl;
    input_parameter << "Time_limit:\t \t" << time_limit << endl;
    input_parameter << "Courant_number:\t \t" << cfl << endl;
    input_parameter << "Integration_method:\t" << integration_method << endl;
    input_parameter << "Derivative_method:\t" << derivative_method << endl;
    input_parameter << "Time_restart:\t" << time_restart << endl;
    input_parameter << '\n';
    input_parameter << "Physical parameter:" << endl;
    input_parameter << '\n';
    input_parameter << "Q_toomre:\t \t" << q_toomre << endl;
    input_parameter << "Sound_Speed:\t \t" << cs_0 << endl;
    input_parameter << "Kepler_prefactor:\t" << kepler_prefactor << endl;
    input_parameter << "Cooling_time:\t \t" << cooling_time << endl;
	input_parameter << "Cooling_mode:\t \t" << cooling_mode << endl;
    input_parameter << "Adiabatic_index:\t" << adiabatic_index << endl;
    input_parameter << "U_equilibrium:\t \t" << U_eq << endl;
    input_parameter << "Sync_UQ:\t \t" << sync_UQ << endl;
    input_parameter << '\n';
    input_parameter << "Initial perturbation:" << endl;
    input_parameter << '\n';
    input_parameter << "Sigma_amp:\t \t" << sigma_amp << endl;
    input_parameter << "Internal_amp:\t" << internal_amp << endl;
    input_parameter << "Ux_amp:\t \t" << ux_amp << endl;
    input_parameter << "Uy_amp:\t" << uy_amp << endl;
    input_parameter << "Frac:\t" << frac << endl;
    input_parameter << "Scaling:\t" << scaling << endl;
    input_parameter << '\n';
    input_parameter << "Switches (On: 1 / Off: 0):\t" << endl; 
    input_parameter << '\n';
    input_parameter << "Switch_compressible:\t" << switch_compressible << endl;
    input_parameter << "Switch_density_grad:\t" << switch_density_grad << endl;
    input_parameter << "Switch_pressure_grad:\t" << switch_pressure_grad << endl;
    input_parameter << "Switch_coriolis:\t" << switch_coriolis << endl;
    input_parameter << "Switch_centrifugal:\t" << switch_centrifugal << endl;
    input_parameter << "Switch_advective:\t" << switch_advective << endl;
    input_parameter << "Switch_cooling:\t\t" << switch_cooling << endl;
    input_parameter << "Switch_energy_grad:\t" << switch_energy_grad << endl;
    input_parameter << "Switch_gravi_grad:\t" << switch_gravi_grad << endl;
    input_parameter << "Switch_viscous_pressure:\t" << switch_viscous_pressure << endl;
    input_parameter << '\n';
    input_parameter << "Damping_coeff_viscous_pressure:\t\t" << damping_coeff_viscous_pressure << endl;
    input_parameter << '\n';
    input_parameter << "Damping_coeff_2_density_radial:\t\t" << damping_coeff_2_density_radial << endl;
    input_parameter << "Damping_coeff_2_density_poloidal:\t" << damping_coeff_2_density_poloidal << endl;
    input_parameter << "Damping_coeff_2_energy_radial:\t\t" << damping_coeff_2_energy_radial << endl;
    input_parameter << "Damping_coeff_2_energy_poloidal:\t" << damping_coeff_2_energy_poloidal << endl;
    input_parameter << "Damping_coeff_2_xvelocity_radial:\t" <<  damping_coeff_2_xvelocity_radial << endl;
    input_parameter << "Damping_coeff_2_xvelocity_poloidal:\t" << damping_coeff_2_xvelocity_poloidal << endl;
    input_parameter << "Damping_coeff_2_yvelocity_radial:\t" << damping_coeff_2_yvelocity_radial << endl;
    input_parameter << "Damping_coeff_2_yvelocity_poloidal:\t" << damping_coeff_2_yvelocity_poloidal << endl;
    input_parameter << "Maximum_damping:\t" << d_max << endl;
    input_parameter << "Damping_flip_point:\t" << damp_flip_point << endl;
    input_parameter << "Damping_sharpness:\t" << damp_sharpness << endl;
    input_parameter << "Damping_Evolution:\t" << damping_check << endl;
    input_parameter << '\n';
    input_parameter << "Output file settings:" << endl;
    input_parameter << '\n';
    input_parameter << "N_output:\t \t \t" << n_output << endl;
    input_parameter << "Save_Restart_Interval:\t \t \t" << restart_interval << endl;
    input_parameter << "Delta_out:\t \t \t" << delta_out << endl;
    input_parameter << "Input_file:\t\t\t" << input_file << endl;
    input_parameter << "Grid_file:\t\t\t" << grid_file << endl;
    input_parameter << "Mean_kinetic_energy:\t\t" << mean_kinetic_energy << endl;
    input_parameter << "Mean_thermal_energy:\t\t" << mean_thermal_energy << endl;
    input_parameter << "Mean_gravitational_energy:\t" << mean_gravitational_energy << endl;
    input_parameter << "Mean_squared_surface_density:\t" << mean_squared_surface_density << endl;
    input_parameter << "XY_gravitational_potential\t" << xy_gravitational_potential << endl;
    input_parameter << "XY_surface_density\t \t" << xy_surface_density << endl;
    input_parameter << "XY_internal_energy\t \t" << xy_internal_energy << endl;
    input_parameter << "XY_radial_velocity\t \t" << xy_radial_velocity << endl;
    input_parameter << "XY_poloidal_velocity\t \t" << xy_poloidal_velocity << endl;
    input_parameter << "X_poloidal_velocity\t \t" << x_poloidal_velocity << endl;
    input_parameter << "Stress\t \t" << stress << endl;
    input_parameter << "Spectrum\t \t" << spectrum << endl;
    input_parameter << "Mean_Q\t \t" << mean_Q << endl;
	input_parameter << "Save_ghost\t \t" << save_ghost << endl;
    input_parameter << '\n';
    input_parameter << "Output file format (On: 1 / Off: 0):" << endl;
    input_parameter << '\n';
    input_parameter << "Gnuplot_format:\t\t\t" << gnuplot_format << endl;
    input_parameter << "General_format:\t\t\t" << general_format << endl;
    input_parameter << '\n';
    input_parameter << "Size of declared arrays:" << endl;
    input_parameter << '\n';
    input_parameter << "Elements in prefactor list 1st derivative:\t" << prefactor_list_1st_derivative.size() << endl;
    input_parameter << "Elements in prefactor list 2nd derivative:\t" << prefactor_list_2nd_derivative.size() << endl;
    input_parameter << "Elements in prefactor list 4th derivative:\t" << prefactor_list_4th_derivative.size() << endl;
    input_parameter << "Elements in combining vector:\t \t \t" << combining_vector.size() << endl;
    input_parameter << "Elements in k radial vector:\t \t \t" << k_radial_vector.size() << endl;
    input_parameter << "Elements in k poloidal vector:\t \t \t" << k_poloidal_vector.size() << endl;
    input_parameter << "Elements in inverse k matrix:\t \t \t" << inverse_k_absolute_matrix.size() << endl;
    input_parameter << "Elements in k store array:\t \t \t" << k_store_array.size() << endl;
    input_parameter << "Elements in viscous pressure array: \t \t" << viscous_pressure_array.size() << endl;
    
    input_parameter.close();
  }
  
  
  // INITIALIZE THE ARRAY 'COMBINING VECTOR' WHICH SUMMARIZES ALL REQUIRED QUANTITIES ON THE GRID AT THE BEGINNING (THE REFERENCE RADIUS IS ASSUMED TO BE IN THE MIDDLE OF THE COMPUTATIONAL DOMAIN)
  
  double k_x = adiabatic_index * PI * equilibrium_density;
  double u_perturbation_amplitude = 1e-5;
  double v_perturbation_amplitude = - 2.0 * u_perturbation_amplitude / (sqrt( (- 4.0 * adiabatic_index - pow(k_x,2.0) + 2.0 * adiabatic_index * PI * k_x * equilibrium_density)/ adiabatic_index ));
  double Sigma_perturbation_amplitude =  k_x * equilibrium_density * u_perturbation_amplitude / (sqrt( (-4.0 * adiabatic_index - pow(k_x,2.0)+ 2.0 * adiabatic_index * PI * k_x * equilibrium_density)/ adiabatic_index ));
  double U_perturbation_amplitude = adiabatic_index * k_x * ( 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre) ) * u_perturbation_amplitude / (sqrt( (-4.0 * adiabatic_index - pow(k_x,2.0) + 2.0 * adiabatic_index * PI * k_x * equilibrium_density)/ adiabatic_index ));
  
  
  
  
  
  // GRID OF SIZE n_radial x n_poloidal CONTAINING RANDOM VALUES. THEY WILL BE USED LATER AS RANDOM PHASE OFFSTES FOR THE INITIAL CONDITION
  
  if(initialization == 15){
  for(int i = 0; i < n_radial; i++)
   {
     for(int j = 0; j < n_poloidal; j++)
     {
	phase[i][j] = 2*PI*((rand() % 100))/100;
     } 
   }
  }
    
    
    
  
  // CHECK WHETHER THE RESTART MODE IS USED ...
  // ... AND LOAD THE RESTART DATA
  // NOTE THAT THE RESTART TIME MUST BE A NATURAL NUMBER OF ORBITS
    
  if(initialization == 16)
  {
    
    stringstream restart_file1;
    restart_file1 << "surface_density_restart_gen_" << time_restart << ".dat";
    fstream datei1;  
    datei1.open(restart_file1.str(), ios::in);
    string zeile1;
    int i = 0;
    while(getline(datei1, zeile1)){
      stringstream zeilenpuffer(zeile1);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,1,n_poloidal) );
      }
      i++;  
    }
    datei1.close();
    
    
    //Here the surface dnesity is rescaled by a factor 'scaling'. That is necessary since one needs to adjust the surface density ...
    //in case one wants to do a restart from one q_toomre-value to another q_toomre-value. 
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		combining_vector.at( c_index_function(i,j,1,n_poloidal) ) *= scaling;
	  }
    }
      
    
    stringstream restart_file2;
    restart_file2 << "internal_energy_restart_gen_" << time_restart << ".dat";
    fstream datei2;
    datei2.open (restart_file2.str(), ios::in); 
    string zeile2;
    i = 0;
    while(getline(datei2, zeile2)){
      stringstream zeilenpuffer(zeile2);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,2,n_poloidal) );
      }
      i++;
    }
    datei2.close();
    
    stringstream restart_file3;
    restart_file3 << "radial_velocity_restart_gen_" << time_restart << ".dat";
    fstream datei3;  
    datei3.open(restart_file3.str(), ios::in);
    string zeile3;
    i = 0;
    while(getline(datei3, zeile3)){
      stringstream zeilenpuffer(zeile3);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,3,n_poloidal) );
      }
      i++;
    }
    datei3.close();
    
    stringstream restart_file4;
    restart_file4 << "poloidal_velocity_restart_gen_" << time_restart << ".dat";
    fstream datei4;  
    datei4.open(restart_file4.str(), ios::in);
    string zeile4;
    i = 0;
    while(getline(datei4, zeile4)){
      stringstream zeilenpuffer(zeile4);
  
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,4,n_poloidal) );
      }
      i++;  
    }
    datei4.close();
    
    stringstream restart_file5;
    restart_file5 << "gravitational_potential_restart_gen_" << time_restart << ".dat";
    fstream datei5;  
    datei5.open(restart_file5.str(), ios::in);
    string zeile5;
    i = 0;
    while(getline(datei5, zeile5)){
      stringstream zeilenpuffer(zeile5);
  
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,0,n_poloidal) );
      }
      i++;
    }
    datei5.close();
      
   }
  
  
  
  
  
  if(initialization == 17)
  {
    
    //'initialization = 17' WORKS SIMILAR TO '16' EXCEPT THAT THE AMPLITUDES OF ALL ...
    //PERTURBATIONS ARE MULTIPLIED BY THE FACTOR 'scaling'. NOTE THTA THAT THE SCALING ...
    //APPLIES ONLY TO THE PERTURBATIONS AND, HENCE, THE BACKGROUND REMAINS THE SAME.
    
    double scale_store = 0.0;		//Will be used as a store for the original values ...
					//read from the restart-files
					
    
    stringstream restart_file1;
    restart_file1 << "surface_density_restart_gen_" << time_restart << ".dat";
    fstream datei1;  
    datei1.open(restart_file1.str(), ios::in);
    string zeile1;
    int i = 0;
    while(getline(datei1, zeile1)){
      stringstream zeilenpuffer(zeile1);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,1,n_poloidal) );
      }
      i++;  
    }
    datei1.close();
   
    //Rescaling of the surface density perturbation-amplitudes
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		scale_store = combining_vector.at( c_index_function(i,j,1,n_poloidal) );
		combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + scaling*( scale_store - equilibrium_density );
	  }
    }
      
    
    
    stringstream restart_file2;
    restart_file2 << "internal_energy_restart_gen_" << time_restart << ".dat";
    fstream datei2;
    datei2.open (restart_file2.str(), ios::in); 
    string zeile2;
    i = 0;
    while(getline(datei2, zeile2)){
      stringstream zeilenpuffer(zeile2);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,2,n_poloidal) );
      }
      i++;
    }
    datei2.close();

    //Rescaling of the internal energy density perturbation-amplitudes
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		scale_store = combining_vector.at( c_index_function(i,j,2,n_poloidal) );
		combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = U_eq + scaling*( scale_store - U_eq );
	  }
    }

    
    
    stringstream restart_file3;
    restart_file3 << "radial_velocity_restart_gen_" << time_restart << ".dat";
    fstream datei3;  
    datei3.open(restart_file3.str(), ios::in);
    string zeile3;
    i = 0;
    while(getline(datei3, zeile3)){
      stringstream zeilenpuffer(zeile3);
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,3,n_poloidal) );
      }
      i++;
    }
    datei3.close();
    
    //Rescaling of the radial velocity perturbation-amplitudes 
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		scale_store = combining_vector.at( c_index_function(i,j,3,n_poloidal) );
		combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = scaling*scale_store;
	  }
    }

    
    
    stringstream restart_file4;
    restart_file4 << "poloidal_velocity_restart_gen_" << time_restart << ".dat";
    fstream datei4;  
    datei4.open(restart_file4.str(), ios::in);
    string zeile4;
    i = 0;
    while(getline(datei4, zeile4)){
      stringstream zeilenpuffer(zeile4);
  
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,4,n_poloidal) );
      }
      i++;  
    }
    datei4.close();
    
    //Rescaling of the poloidal velocity perturbation-amplitudes
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		scale_store = combining_vector.at( c_index_function(i,j,4,n_poloidal) );
		combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + scaling*( scale_store + (3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial );
	  }
    }

    
    
    stringstream restart_file5;
    restart_file5 << "gravitational_potential_restart_gen_" << time_restart << ".dat";
    fstream datei5;  
    datei5.open(restart_file5.str(), ios::in);
    string zeile5;
    i = 0;
    while(getline(datei5, zeile5)){
      stringstream zeilenpuffer(zeile5);
  
      for (int j = 0; j < n_poloidal; j++){
	zeilenpuffer >> combining_vector.at( c_index_function(i,j,0,n_poloidal) );
      }
      i++;
    }
    datei5.close();
    
    //Rescaling of the gravitational potential perturbation-amplitudes
    for(int i = 0; i < n_radial; i++)
    {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		scale_store = combining_vector.at( c_index_function(i,j,0,n_poloidal) );
		combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = scaling*scale_store;
	  }
    }
    
    
      
   }
   

  
  
  
  // INITIALIZE THE COMBINING VECTOR
  
  for(int i = 0; i < n_radial; i++)
  {
    for(int j = 0; j < n_poloidal; j++)
    {
      // INIITIALIZE THE STANDARD RUN SETTINGS FROM GAMMIE: EQUILIBRIUM SOLUTION AND A SMALL WAVE PERTURBATION IN THE SURFACE DENSITY
      
      if(initialization == 1)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 0.0001 * cos(2.0 * PI/box_length_poloidal * (0.5 + j - 0.5 * n_poloidal) * delta_poloidal - 4.0 * PI/box_length_radial * (0.5 + i - 0.5 * n_radial) * delta_radial);
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * 1.0/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      // INITIALIZE COMPUTATIONAL DOMAIN WITH THE EQUILIBRIUM SOLUTION AND A SINGLE SMALL HARMONIC WAVE PERTURBATION IN THE SURFACE DENSITY ALONG THE POLOIDAL (Y) DIRECTION 
      
      if(initialization == 2)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + sigma_amp * cos( 2.0 * PI / box_length_poloidal * (0.5 + j - 0.5 * n_poloidal) * delta_poloidal );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * 1.0/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      // INITIALIZE COMPUTATIONAL DOMAIN WITH THE EQUILIBRIUM SOLUTION AND A SINGLE SMALL HARMONIC WAVE PERTURBATION IN THE SURFACE DENSITY ALONG THE RADIAL (X) DIRECTION 
      
      if(initialization == 3)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 4e-5 * cos( (2.0 * sqrt(adiabatic_index)/ q_toomre) * (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre) + 5e-5 * cos( (2.0 * sqrt(adiabatic_index)/ q_toomre) * (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 2e-5 * cos( (2.0 * sqrt(adiabatic_index)/ q_toomre) * (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + 2e-5 * cos( (2.0 * sqrt(adiabatic_index)/ q_toomre) * (0.5 + i - 0.5 * n_radial) * delta_radial );
      }
      
      // INITIALIZE COMPUTATIONAL DOMAIN WITH THE EQUILIBRIUM SOLUTION AND A WHITE NOISE VELOCITY PERTURBATION ACCORDING TO GAMMIE SO THAT THE AMPLITUDE IS RATHER SMALL
      
      if(initialization == 4)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density;
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = ( 1.0 / (adiabatic_index * (adiabatic_index - 1.0)) ) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = ux_amp * ( (rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + uy_amp * ( (rand() % 100 + 1) - 50);
      }
      
      // INITIALIZE COMPUTATIONAL DOMAIN WITH THE EQUILIBRIUM SOLUTION AND A GAUSSIAN SURFACE DENSITY PERTURBATION 
      
      if(initialization == 5)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = 1e-1* exp( - ( pow( (0.5 + j - 0.5 * n_poloidal) * delta_poloidal, 2.0)) / 0.7 );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      // INIITIALIZE COMPUTATIONAL DOMAIN WITH THE EQUILIBRIUM SOLUTION AND A SMALL WHITE NOISE PERTURBATION IN THE SURFACE DENSITY
      
      if(initialization == 6)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + sigma_amp * ((rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = (pow(cs_0, 3.0)/ (PI * q_toomre)) * 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) + internal_amp * ((rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = ux_amp * ( (rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + uy_amp * ( (rand() % 100 + 1) - 50);
      }
      
      if(initialization == 7)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 1e-2 * sin( (2.0 * PI / 10.0) * (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 8)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = 0.1 * exp( - ( pow( (0.5 + j - 0.5 * n_poloidal) * delta_poloidal, 2.0) + pow((0.5 + i - 0.5 * n_radial) * delta_radial , 2.0) ) / 0.04 );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 9)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density - Sigma_perturbation_amplitude * cos( k_x* (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre) - U_perturbation_amplitude * cos( k_x* (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = u_perturbation_amplitude *  sin( k_x* (0.5 + i - 0.5 * n_radial) * delta_radial );
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + v_perturbation_amplitude * sin( k_x* (0.5 + i - 0.5 * n_radial) * delta_radial );
      }
      
      if(initialization == 10)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 1e-5 * cos(2.0 * PI/box_length_radial * (0.5 + i - 0.5 * n_radial) * delta_radial);
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 11)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	if(i > n_radial/2 - 10 && i < n_radial/2 + 10 && j > n_poloidal/2 - 10 && j < n_poloidal/2 + 10)
	{
	  combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 0.05;
	}
	else
	{
	  combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density;
	}
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.2;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 12)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 1e-3* exp( - ( pow( (0.5 + i - 0.5 * n_radial) * delta_radial, 2.0)) / 3.0 );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 13)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + 0.001 * exp( - ( pow( (0.5 + j - 224) * delta_poloidal, 2.0) + pow((0.5 + i - 224) * delta_radial , 2.0) ) / 0.025 );
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 1.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      if(initialization == 14)
      {
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density;
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial;
      }
      
      
      if(initialization == 15)
      {
	double base = 0.0;
	int noise_limit_rad = floor(n_radial/frac);
	int noise_limit_pol = floor(n_poloidal/frac);
	
	for(int l = 0; l < noise_limit_rad; l++)
	{
	  for(int m = 0; m < noise_limit_pol; m++)
	  {
	    base = base + sigma_amp*cos( 2.0 * (l+1) * PI/box_length_radial * (0.5 + i - 0.5 * n_radial) * delta_radial  +  2.0 * (m+1) * PI / box_length_poloidal * (0.5 + j - 0.5 * n_poloidal) * delta_poloidal  +  phase[l][m]);
	  }
	}
	
	
	combining_vector.at( c_index_function(i,j,0,n_poloidal) ) = 0.0;
	combining_vector.at( c_index_function(i,j,1,n_poloidal) ) = equilibrium_density + base;
	combining_vector.at( c_index_function(i,j,2,n_poloidal) ) = (pow(cs_0, 3.0)/ (PI * q_toomre)) * 1.0/ (adiabatic_index * (adiabatic_index - 1.0)) + internal_amp * ((rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,3,n_poloidal) ) = ux_amp * ( (rand() % 100 + 1) - 50);
	combining_vector.at( c_index_function(i,j,4,n_poloidal) ) = -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial + uy_amp * ( (rand() % 100 + 1) - 50);
	
      }
      
      
      
    }
  }
  
  
  
  //FILL THE GHOST-VECTOR FOR THE FIRST TIME (INNER PART)
  for(int i = 0; i < n_radial; i++)
  {
	  for(int j = 0; j < n_poloidal; j++)
	  {
		  for(int k = 0; k < 5; k++)
		  {
			ghost.at( ghost_index(i + n_ghost, j + n_ghost, k, n_poloidal, n_ghost) ) = combining_vector.at( c_index_function(i, j, k, n_poloidal) );
		  }
	  }
  }
  
  //OUTER PART
  fill_the_ghost(ghost, n_radial, n_poloidal, n_ghost, delta_radial, delta_poloidal, box_length_radial, box_length_poloidal, v_boost_poloidal, 0.0);
  
  
  
  
  // WRITE INITIAL PERTURBATIONS IN A FILE
  // THIS ALLOWS FOR REPEATING THE SIMULATION WITH EXACTLY THE SAME RESULTS
  
  
  if(initialization != 16 && initialization != 17)
  {
  
  
    // GRAVIATIONAL POTENTIAL
		      
    
    stringstream init1;
    init1 << "gravitational_potential_init" << ".dat";
    write_xy_data_gen(init1.str(), combining_vector, n_radial, n_poloidal, 0, box_length_radial, box_length_poloidal);



	
	
	
  // SURFACE DENSITY

    
    stringstream init2;
    init2 << "surface_density_init" << ".dat";
    write_xy_data_gen(init2.str(), combining_vector, n_radial, n_poloidal, 1, box_length_radial, box_length_poloidal);

    



  // INTERNAL ENERGY

    stringstream init3;
    init3 << "internal_energy_init" << ".dat";
    write_xy_data_gen(init3.str(), combining_vector, n_radial, n_poloidal, 2, box_length_radial, box_length_poloidal);

    



  // RADIAL VELOCITY

    
    stringstream init4;
    init4 << "radial_velocity_init" << ".dat";
    write_xy_data_gen(init4.str(), combining_vector, n_radial, n_poloidal, 3, box_length_radial, box_length_poloidal);






  // POLOIDAL VELOCITY

    
    stringstream init5;
    init5 << "poloidal_velocity_init" << ".dat";
    write_xy_data_gen(init5.str(), combining_vector, n_radial, n_poloidal, 4, box_length_radial, box_length_poloidal);

  
  }
	 
	  

  
  
   
  // WRITE INITIALIZED DATA IN FILE
  
  
  if(gnuplot_format)
  {
    fstream gnuplot_data;
    gnuplot_data.open ("initial_density.dat", ios::out);
    //gnuplot_data << fixed << setprecision(3) << endl;
    for(int i = 0; i < n_radial; i++)
    {
      for(int j = 0; j < n_poloidal; j++)
      {
	gnuplot_data << ((i + 1.0)/n_radial) * box_length_radial << ' ' << ((j + 1.0)/n_poloidal) * box_length_poloidal << ' ' << combining_vector.at( c_index_function(i,j,1,n_poloidal) ) << endl; 
      }
      gnuplot_data << endl;
    }
    gnuplot_data.close();
  }

  
  // DETERMINE THE K-VECTORS CORRESPONDING TO THE GRID, THEY WILL BE CONSTANT OVER RUNTIME BECAUSE THE GRID IS FIXED
  
  
  k_vector_components_of_grid(k_radial_vector, k_poloidal_vector, inverse_k_absolute_matrix, n_radial, n_poloidal, box_length_radial, box_length_poloidal, grid_file);
  
  
  // DEFINE COUNTER IN SUCH A WAY THAT ITS CURRENT VALUE CORRESPONDS TO THE NUMER OF TIME LOOP CYCLES (USE IT TO WRITE VALUES IN FILES AT CERTAIN POINTS IN TIME)
  
  
  double time = 0.0;			//Variable containing the current physical time
  int percentage = 10;			//cout << every percentage << endl;
  double last_output_time = 0.0;	//Save the physical time of the last output in order to make restarts easier
  time_step_old = time_step;		//Store-variable for the old timestep. Needed in order to check whether the diagnostic files have to be written
  
 
  
  
  // GO IN THE TIME LOOP. EACH RUN THROUGH THE LOOP REPRESENTS A SINGLE ITERATION OF THE GOVERNING EQUATIONS
  auto start = high_resolution_clock::now();
  
  
  
  
  
  
  cout << "ATTENTION! MAKE SURE THE DAMPING-COEFFICIENTS ARE CHOSEN APPROPRIATELY" << endl;
  cout << "INITIALIZATION USED IS " << initialization << endl;
  cout << "CHOSEN COOLING MODE " << cooling_mode << endl;
  
  if(cooling_mode < 0 || cooling_mode > 1) {
	  cout << "ATTENTION!  cooling_mode  SHOULD BE CHOSEN BETWEEN  0 and 1" << endl;
  }
  
  
  
  
  
  while( time <= time_limit )
  {   
    
    
    
    // CHECK WHETHER A DIAGNOSTIC IS REQUIRED
    
    if( time - floor(time/delta_out)*delta_out <= time_step_old && time != 0.0 )
    {
      
      // WRITE OUTPUT TIMES INTO A FILE "time.dat"
     
      fstream time_output;
      time_output.open ("time.dat", ios::out | ios::app);
      time_output << time_restart + floor(time/delta_out)*delta_out << endl;
      time_output.close();
      
      
	 // AVERAGED GRAVITATIONAL ENERGY
      
	 if(mean_gravitational_energy)
	  {
	    stringstream filename;
	    filename << "mean_gravitational_energy.dat";
	    write_averaged_gravitational_energy(filename.str(), combining_vector,time, q_toomre, adiabatic_index, n_radial, n_poloidal);
	  }
	  
	  
	  
	  // AVERAGED KINETIC ENERGY
	  
	  
	  
	  if(mean_kinetic_energy)
	  {
	    stringstream filename;
	    filename << "mean_kinetic_energy.dat";
	    write_averaged_kinetic_energy(filename.str(), combining_vector, time, q_toomre, adiabatic_index, delta_radial, n_radial, n_poloidal);
	  }
	  
	  
	  
	  // AVERAGED THERMAL/INTERNAL ENERGY
	  
	  
	  mean_U = 0.0;
	  
	  if(mean_thermal_energy)
	  {
	    stringstream filename;
	    filename << "mean_thermal_energy.dat";
	    write_averaged_internal_energy(filename.str(), combining_vector, time, q_toomre, adiabatic_index, n_radial, n_poloidal);
	  }
	  
	  
	  
	  // MASS CONSERVATION CHECK
	  
	  
	  
	  if(mass_conservation_check)
	  {
	    stringstream filename;
	    filename << "mass_conservation.dat";
	    write_mass_conservation_check(filename.str(), combining_vector, time, q_toomre, adiabatic_index, n_radial, n_poloidal);

	  }
	  
	  
	  
	  // AVERAGED SUQARED SURFACE DENSITY
	  
	  
	  
	  if(mean_squared_surface_density)
	  {
	    stringstream filename;
	    filename << "surface_density_norm.dat";
	    write_averaged_squared_modulus(filename.str(), combining_vector, time, q_toomre, adiabatic_index, n_radial, n_poloidal, 1);
	  }
	
      
      
	  // GRAVIATIONAL POTENTIAL
	  
	  
	  
	  if(xy_gravitational_potential)
	  {
	    if(gnuplot_format)
	    {
	      stringstream filename;
	      // \note Instead of counter, also time could be used.
	      filename << "gravitational_potential_gnu_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gnu(filename.str(), combining_vector, n_radial, n_poloidal, 0, box_length_radial, box_length_poloidal);
	    }
	    
	    if(general_format)
	    {
	      stringstream filename;
	      filename << "gravitational_potential_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gen(filename.str(), combining_vector, n_radial, n_poloidal, 0, box_length_radial, box_length_poloidal);
	    }
	    
	  }
		
		
		
	  // SURFACE DENSITY
	  
	  
	  
	  if(xy_surface_density)
	  {
	    if(gnuplot_format)
	    {
	      stringstream filename;
	      // \note Instead of counter, also time could be used.
	      filename << "surface_density_gnu_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gnu(filename.str(), combining_vector, n_radial, n_poloidal, 1, box_length_radial, box_length_poloidal);
	    }
	    
	    if(general_format)
	    {
	      stringstream filename;
	      filename << "surface_density_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gen(filename.str(), combining_vector, n_radial, n_poloidal, 1, box_length_radial, box_length_poloidal);
	    }
	    
	  }
	  
	  
	  
	  // INTERNAL ENERGY
	  
	  
	  
	  if(xy_internal_energy)
	  {
	    if(gnuplot_format)
	    {
	      stringstream filename;
	      // \note Instead of counter, also time could be used.
	      filename << "internal_energy_gnu_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gnu(filename.str(), combining_vector, n_radial, n_poloidal, 2, box_length_radial, box_length_poloidal);
	    }
	    
	    if(general_format)
	    {
	      stringstream filename;
	      filename << "internal_energy_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gen(filename.str(), combining_vector, n_radial, n_poloidal, 2, box_length_radial, box_length_poloidal);
	    }
	    
	  }
	  
	  
	  
	  // RADIAL VELOCITY
	  
	  
	  
	  if(xy_radial_velocity)
	  {
	    if(gnuplot_format)
	    {
	      stringstream filename;
	      // \note Instead of counter, also time could be used.
	      filename << "radial_velocity_gnu_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gnu(filename.str(), combining_vector, n_radial, n_poloidal, 3, box_length_radial, box_length_poloidal);
	    }
	    
	    if(general_format)
	    {
	      stringstream filename;
	      filename << "radial_velocity_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gen(filename.str(), combining_vector, n_radial, n_poloidal, 3, box_length_radial, box_length_poloidal);
	    }
	    
	  }
	  
	  
	  
	  // POLOIDAL VELOCITY
	  
	  
	  
	  if(xy_poloidal_velocity)
	  {
	    if(gnuplot_format)
	    {
	      stringstream filename;
	      // \note Instead of counter, also time could be used.
	      filename << "poloidal_velocity_gnu_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gnu(filename.str(), combining_vector, n_radial, n_poloidal, 4, box_length_radial, box_length_poloidal);
	    }
	    
	    if(general_format)
	    {
	      stringstream filename;
	      filename << "poloidal_velocity_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
	      write_xy_data_gen(filename.str(), combining_vector, n_radial, n_poloidal, 4, box_length_radial, box_length_poloidal);
	    }
	    	
	  }
	  

	  if(x_poloidal_velocity)
	  {
	    stringstream filename;
	    filename << "poloidal_velocity_rad.dat";
	    write_x_data_averaged_over_y(filename.str(), combining_vector, n_radial, n_poloidal, 4);
	  }
	  
	  
	  
	  
	  
	  // AVERAGED RAYNOLDS_STRESS
	  
	  
	  
	  if(stress)
	  { 
	    
	    rey_stress = 0.0;
	    mean_U = 0.0;
      
	    for(int i = 0; i < n_radial; i++)
	    {
	      for(int j = 0; j < n_poloidal; j++)
	      {
		rey_vel_only += combining_vector.at( c_index_function(i,j,3,n_poloidal)) * ( combining_vector.at( c_index_function(i,j,4,n_poloidal)) + (3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial );
		rey_stress += combining_vector.at( c_index_function(i,j,1,n_poloidal)) * combining_vector.at( c_index_function(i,j,3,n_poloidal)) * ( combining_vector.at( c_index_function(i,j,4,n_poloidal)) + (3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial );
		rey_stress_0 += combining_vector.at( c_index_function(i,j,1,n_poloidal)) * combining_vector.at( c_index_function(i,j,3,n_poloidal)) * ( -(3.0/2.0) * (0.5 + i - 0.5 * n_radial) * delta_radial );
		mean_U += combining_vector.at( c_index_function(i,j,2,n_poloidal));
	      }
	    }
	    
	    rey_stress /= (n_radial*n_poloidal);
	    rey_stress_0 /= (n_radial*n_poloidal);
	    mean_U /= (n_radial*n_poloidal);
	    alpha_norm = adiabatic_index*(adiabatic_index-1.0)*mean_U;	
      
	    fstream turb_stress;
	    
	    turb_stress.open("reynolds_stress.dat", ios::out | ios::app);
  
	    turb_stress << time_restart+floor(time/delta_out)*delta_out << '\t' << rey_stress << '\t' << rey_stress/alpha_norm << '\t' << rey_stress_0 << '\t' << rey_stress_0/alpha_norm << '\t' << rey_vel_only << endl;
  
	    turb_stress.close();
	  }
	  
	  
	  
	  
	  
	  // DIAGNOSTIC FOR CALCULATING THE SPATIAL AVERAGE OF THE MOMENTARY TOOMRE-Q
	  
	  
	  
	  if(mean_Q)
	  { 
	    
	    q_cumulated = 0.0;
	    sonic_cumulated = 0.0;
      
	    for(int i = 0; i < n_radial; i++)
	    {
	      for(int j = 0; j < n_poloidal; j++)
	      {
		q_cumulated += (1/PI) * sqrt( adiabatic_index * (adiabatic_index-1.0) * combining_vector.at( c_index_function(i,j,2,n_poloidal)) / pow( combining_vector.at( c_index_function(i,j,1,n_poloidal)) , 3.0 ) );
		sonic_cumulated += sqrt( adiabatic_index * (adiabatic_index-1.0) * combining_vector.at( c_index_function(i,j,2,n_poloidal)) / combining_vector.at( c_index_function(i,j,1,n_poloidal)) );
	      }
	    }
	    
	    q_cumulated /= (n_radial*n_poloidal);
	    sonic_cumulated /= (n_radial*n_poloidal);
      
	    fstream q_diagnostic;
	    
	    q_diagnostic.open("Q_evolution.dat", ios::out | ios::app);
  
	    q_diagnostic << time_restart+floor(time/delta_out)*delta_out << '\t' << q_cumulated << '\t' << sonic_cumulated << endl;
  
	    q_diagnostic.close();
	  }
	  
	  
	  
	  
	  if(damping_check)
	  {
	    
	    fstream check_damp;
	    check_damp.open("damping_evolution.dat", ios::out | ios::app);
	    check_damp << time_restart+floor(time/delta_out)*delta_out << '\t' << damping_coeff_2_density_radial << endl;
	    check_damp.close();
	    
	  }
	  
	  
	  
	  
	  
	  
	  // THE FOLLOWING DIAGNOSTIC SAVES THE VALUES AT ALL GRID POINTS INCLUDING THE GHOST ZONES. 
	  // THE OUTPUT IS FOR ALL FIELD VARIABLES (Phi, d, U, vx, vy).
	  // THE DIAGNOSTIC IS INTENDED/USEFUL FOR DEBUGGING REASONS.
	  if(save_ghost) // save_ghost must be set true in the input-file in order to use the diagnostic
	  {
		stringstream filename0;
		filename0 << "gravitational_potential_ghost_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
		write_ghost_data_gen(filename0.str(), ghost, n_radial, n_poloidal, n_ghost, 0, box_length_radial, box_length_poloidal);
		
		stringstream filename1;
		filename1 << "surface_density_ghost_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
		write_ghost_data_gen(filename1.str(), ghost, n_radial, n_poloidal, n_ghost, 1, box_length_radial, box_length_poloidal);
		
		stringstream filename2;
		filename2 << "internal_energy_ghost_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
		write_ghost_data_gen(filename2.str(), ghost, n_radial, n_poloidal, n_ghost, 2, box_length_radial, box_length_poloidal);
		
		stringstream filename3;
		filename3 << "radial_velocity_ghost_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
		write_ghost_data_gen(filename3.str(), ghost, n_radial, n_poloidal, n_ghost, 3, box_length_radial, box_length_poloidal);
		
		stringstream filename4;
		filename4 << "poloidal_velocity_ghost_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
		write_ghost_data_gen(filename4.str(), ghost, n_radial, n_poloidal, n_ghost, 4, box_length_radial, box_length_poloidal);
	  }
	  
	  
	  
	  
	  
	  //stringstream filename;
	  //filename << "amplitude.dat";
	  //write_wave_amplitude(filename.str(), combining_vector, time, q_toomre, adiabatic_index, n_radial, n_poloidal);
	  
	  
    }
    
    
    
    
    
    
    
    // DETERMINE THE CURRENT SHEARING OF IMAGINARY ADJACENT SHEARING BOXES AT EACH TIME LOOP CYCLE, RESET VALUE EVERY TIME WHEN SHEAR BOXES ARE PERIODIC
    
    
    double time_periodic = (2.0/3.0) * (box_length_poloidal/box_length_radial) * floor( (3.0/2.0) * (box_length_radial/box_length_poloidal) * time ); 
    double shearing_poloidal = kepler_prefactor * box_length_radial * (time - time_periodic);
    
    
    
    
    
    // CALL FUNCTION TO DETERMINE THE GRAVITATIONAL POTENTIAL VIA FAST FOURIER TRANSFORM OF THE SURFACE DENSITY
    
    fft_solve_poisson_equation(p_forward_transform, p_backward_transform, in_array, out_array_forward, out_array_backward, combining_vector, inverse_k_absolute_matrix, k_radial_vector, k_poloidal_vector, equilibrium_density, n_radial, n_poloidal, time, delta_radial, delta_poloidal, box_length_radial, box_length_poloidal, n_output, alpha_norm, spectrum);
    
    
   
    
    
    
    
    // DETERMINE CURRENT VISCOUS PRESSURE FOR EACH GRID POINT
    
    
    if(switch_viscous_pressure)
    {
      for(int i = 0; i < n_radial; i++)
      {
	for(int j = 0; j < n_poloidal; j++)
	{
	  // VISCOUS PRESSURE AT EACH GRID POINT IS A DAMPING COEFFICIENT, WHICH IS PROPORTIONAL TO deltaRadial * deltaPoloidal, TIMES THE SQUARED DIVERGENCE AT THE ACCORDING GRID POINT, I. E. damping * density * ( (d_x) u + (d_y) v )²
	
	  viscous_pressure_array.at(j + n_poloidal * i) = damping_coeff_viscous_pressure * combining_vector.at( c_index_function(i,j,1,n_poloidal) ) * pow( ( radial_1st_derivative(ghost, prefactor_list_1st_derivative, i + n_ghost, j + n_ghost, 3, n_poloidal, n_radial, delta_radial, delta_poloidal, derivative_method, shearing_poloidal, v_boost_poloidal) + poloidal_1st_derivative(ghost, prefactor_list_1st_derivative, i + n_ghost, j + n_ghost, 4, n_poloidal, delta_poloidal, derivative_method) ) ,2.0);
	}
      }
    }
    
    
    // CALL FUNCTION TO TIME-INTEGRATE TO DETERMINE THE NEW VELOCITY, SURFACE DENSITY, INTERNAL ENERGY (EULER OR RUNGE KUTTA 4TH ORDER)
    
    
    if(integration_method == 1)
    {
      euler_scheme(combining_vector, viscous_pressure_array, k_store_array, prefactor_list_1st_derivative, prefactor_list_2nd_derivative, prefactor_list_4th_derivative, time_step, n_radial, n_poloidal, delta_radial, delta_poloidal, shearing_poloidal, v_boost_poloidal, derivative_method, integration_method, q_toomre, adiabatic_index, cooling_time, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad,  damping_coeff_2_density_radial,damping_coeff_2_density_poloidal,damping_coeff_2_energy_radial,damping_coeff_2_energy_poloidal, damping_coeff_2_xvelocity_radial, damping_coeff_2_xvelocity_poloidal, damping_coeff_2_yvelocity_radial, damping_coeff_2_yvelocity_poloidal, U_eq, switch_viscous_pressure);
    }
    if(integration_method == 3)
    {
      runge_kutta_3rd_order(combining_vector, viscous_pressure_array, k_store_array, prefactor_list_1st_derivative, prefactor_list_2nd_derivative, prefactor_list_4th_derivative, time_step, n_radial, n_poloidal, delta_radial, delta_poloidal, shearing_poloidal, v_boost_poloidal, derivative_method, integration_method, q_toomre, adiabatic_index, cooling_time, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad,  damping_coeff_2_density_radial,damping_coeff_2_density_poloidal,damping_coeff_2_energy_radial,damping_coeff_2_energy_poloidal, damping_coeff_2_xvelocity_radial, damping_coeff_2_xvelocity_poloidal, damping_coeff_2_yvelocity_radial, damping_coeff_2_yvelocity_poloidal, U_eq, switch_viscous_pressure);
    }
    if(integration_method == 4)
    {
      runge_kutta_4th_order(combining_vector, ghost, viscous_pressure_array, k_store_array, prefactor_list_1st_derivative, prefactor_list_2nd_derivative, prefactor_list_4th_derivative, time_step, n_radial, n_poloidal, n_ghost, delta_radial, delta_poloidal, shearing_poloidal, v_boost_poloidal, derivative_method, integration_method, q_toomre, adiabatic_index, cooling_time, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, damping_coeff_2_density_radial,damping_coeff_2_density_poloidal,damping_coeff_2_energy_radial,damping_coeff_2_energy_poloidal,  damping_coeff_2_xvelocity_radial, damping_coeff_2_xvelocity_poloidal, damping_coeff_2_yvelocity_radial, damping_coeff_2_yvelocity_poloidal, U_eq, switch_viscous_pressure, time);
    }
    
    
    //counter++;
    
    
    // WRITE PROGRAM STATUS OF COMPLETION IN CONSOLE
    
    
    if( (100.0 * time)/(time_limit) >= percentage )
    {
      cout << "Program status: " << percentage << "% completed ..." << endl;
      
      percentage = percentage + 10;
    }
    
    
    
    
    
    //CALCULATING THE NEW TIME STEP BY MEANS OF THE CFL-CRITERION
    
    time_step_old = time_step;  //Store the old time step in a variable "time_step_old". 
    time += time_step;		//Update the current time
	
	vx_max = 1.0;
	vy_max = 1.0;
	
	//Search for the maximum velocity in both radial and poloidal direction
	for( int i = 0; i < n_radial; i++ )
	{
		for( int j = 0; j < n_poloidal; j++ )
		{
	 		if( abs(combining_vector.at( c_index_function(i,j,3,n_poloidal) )) > vx_max )
			{
				vx_max = abs(combining_vector.at( c_index_function(i,j,3,n_poloidal) ));
			}
			if( abs(combining_vector.at( c_index_function(i,j,4,n_poloidal) )) > vy_max )
			{
				vy_max = abs(combining_vector.at( c_index_function(i,j,4,n_poloidal) ));
	 		}
		}
	}
	
	
	//Calculate the new time_step via the advection-CFL-criterion
	
	time_step = cfl * min( box_length_radial/n_radial, box_length_poloidal/n_poloidal ) / (vx_max + vy_max);
	
	
	
	
	
	
	//VISCOSITY-REGULTOR
	//THE ARTIFICIAL VISCOSITY IS SET TO  "d_old"  AND A DYNAMICAL RANGE  "damp_range"  IS USED IN ORDER TO APPLY A HIGHER DAMPING ...
	//... IN CASE THE RADIAL VELOCITY EXEEDS A TRESHOLD  "damp_flip_point".
	
	damping_coeff_2_density_radial = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));																
	damping_coeff_2_density_poloidal = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));														
	damping_coeff_2_energy_radial = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));																
	damping_coeff_2_energy_poloidal = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));																
	damping_coeff_2_xvelocity_radial = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));															
	damping_coeff_2_xvelocity_poloidal = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));														
	damping_coeff_2_yvelocity_radial = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));															
	damping_coeff_2_yvelocity_poloidal = d_old + 0.5 * damp_range * ( 1 + tanh( damp_sharpness * (vx_max - damp_flip_point) ));
	
	
	
	
	
	
	//WRITE RESTART FILES AT TIME POINTS "RESTART_INTERVAL * DELTA_OUT"
	//THIS IS USEFUL AS ONE MIGHT NOT WANT TO SAVE ALL QUANTITIES AT ALL TIME POINTS AS THE SIZE OF THE DATA PRODUCED THIS WAY ...
	//... CAN REACH VERY HIGH VALUES
	
	
	if( time - floor(time/(restart_interval*delta_out))*restart_interval*delta_out <= time_step_old )
	{
	      
	  stringstream filename1;
	  filename1 << "gravitational_potential_restart_gen_" << time_restart+floor(time/(restart_interval*delta_out))*restart_interval*delta_out << ".dat";
	  write_xy_data_gen(filename1.str(), combining_vector, n_radial, n_poloidal, 0, box_length_radial, box_length_poloidal);
		  
	  stringstream filename2;
	  filename2 << "surface_density_restart_gen_" << time_restart+floor(time/(restart_interval*delta_out))*restart_interval*delta_out << ".dat";
	  write_xy_data_gen(filename2.str(), combining_vector, n_radial, n_poloidal, 1, box_length_radial, box_length_poloidal);
		  
	  stringstream filename3;
	  filename3 << "internal_energy_restart_gen_" << time_restart+floor(time/(restart_interval*delta_out))*restart_interval*delta_out << ".dat";
	  write_xy_data_gen(filename3.str(), combining_vector, n_radial, n_poloidal, 2, box_length_radial, box_length_poloidal);
		  
	  stringstream filename4;
	  filename4 << "radial_velocity_restart_gen_" << time_restart+floor(time/(restart_interval*delta_out))*restart_interval*delta_out << ".dat";
	  write_xy_data_gen(filename4.str(), combining_vector, n_radial, n_poloidal, 3, box_length_radial, box_length_poloidal);
		  
	  stringstream filename5;
	  filename5 << "poloidal_velocity_restart_gen_" << time_restart+floor(time/(restart_interval*delta_out))*restart_interval*delta_out << ".dat";
	  write_xy_data_gen(filename5.str(), combining_vector, n_radial, n_poloidal, 4, box_length_radial, box_length_poloidal);

	}
	
	
	
  }
  
  
  
  //WRITE FINAL RESTART FILES AT THE END OF THE SIMULATION
  
  last_output_time = floor(time/delta_out)*delta_out;
	      
  stringstream filename1;
  filename1 << "gravitational_potential_restart_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
  write_xy_data_gen(filename1.str(), combining_vector, n_radial, n_poloidal, 0, box_length_radial, box_length_poloidal);
	  
  stringstream filename2;
  filename2 << "surface_density_restart_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
  write_xy_data_gen(filename2.str(), combining_vector, n_radial, n_poloidal, 1, box_length_radial, box_length_poloidal);
	  
  stringstream filename3;
  filename3 << "internal_energy_restart_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
  write_xy_data_gen(filename3.str(), combining_vector, n_radial, n_poloidal, 2, box_length_radial, box_length_poloidal);
	  
  stringstream filename4;
  filename4 << "radial_velocity_restart_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
  write_xy_data_gen(filename4.str(), combining_vector, n_radial, n_poloidal, 3, box_length_radial, box_length_poloidal);
	  
  stringstream filename5;
  filename5 << "poloidal_velocity_restart_gen_" << time_restart+floor(time/delta_out)*delta_out << ".dat";
  write_xy_data_gen(filename5.str(), combining_vector, n_radial, n_poloidal, 4, box_length_radial, box_length_poloidal);
  
  
  cout << "For Restart --- Time of last output: " << time_restart + last_output_time << endl;
  
  
  
  
  
  // THERE FOLLOWS A PART TO CHECK TOTAL RUNTIME AND FFT-RUNTIME (MAYBE USEFUL FOR PERFOMANCE CHECKS)
  
  auto end = high_resolution_clock::now();
  total_time = duration_cast<milliseconds>(end-start).count();
  
  cout << "Time total: " << total_time/(1000.0) << " s" << endl;
  
  
  
  // DELETE ALLOCATED HEAP MEMORY FOR FFTW3
  
  
  fftw_free(in_array);
  fftw_free(out_array_forward);
  fftw_free(out_array_backward);
  
  
  // CLEAR ALL CREATED PLANS
  
  
  fftw_destroy_plan(p_forward_transform);
  fftw_destroy_plan(p_backward_transform);
  
  cout << "PROGRAM SUCCESSFULLY COMPLETED." << endl;
  
  return 0;
}







/* ##########################################################################
 * End of main function, in the following all required functions are defined 
 * ##########################################################################
 */








// FUNCTION TO FIND THE AMPLITUDE OF A WAVE AT EACH n_output AND WRITE IN FILE

void write_wave_amplitude(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol)
{
  double amplitude = 0.0;
  
  fstream output;
  output.open(filename.c_str(), ios::out | ios::app);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      if(com_vec.at( c_index_function(i,j,1,n_pol)) > amplitude)
      {
	amplitude = com_vec.at( c_index_function(i,j,1,n_pol));
      }
    }
  }
  
  amplitude -= (cs_0/( PI * q_toomre));
  
  amplitude /= (cs_0/( PI * q_toomre));
  
  output << time_restart + time << '\t' << amplitude << endl;
  
  output.close();
}



// FUNCTION TO CHECK IF THE MASS IS CONSERVED ON THE COMPUTATIONAL DOMAIN



void write_mass_conservation_check(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol)
{
  double mass = 0.0;
  
  fstream output;
  output.open(filename.c_str(), ios::out | ios::app);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      mass += ( com_vec.at( c_index_function(i,j,1,n_pol) ) -  cs_0/( PI * q_toomre) );
    }
  }

  mass /= (n_rad * n_pol);
  
  output << mass << endl;
  
  output.close();
}



// FUNCTION THAT WRITES THE AVERAGED KINETIC ENERGY AT EACH n_output IN FILE 



void write_averaged_kinetic_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, double delta_rad, int n_rad, int n_pol)
{
  double sum = 0.0;
  
  fstream output;
  output.open(filename.c_str(), ios::out | ios::app);
  
//  for(int i = 0; i < n_rad; i++)
//  {
//    for(int j = 0; j < n_pol; j++)
//    {
//      sum += ( com_vec.at( c_index_function(i,j,1,n_pol)) * ( pow(com_vec.at(c_index_function(i,j,3,n_pol)), 2.0) + pow(com_vec.at(c_index_function(i,j,4,n_pol)), 2.0) ) - 1.0/( PI * q_toomre) *  pow(-(3.0/2.0) * (0.5 + i - 0.5 * n_rad) * delta_rad, 2.0) );
//    }
//  }
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      sum += ( com_vec.at( c_index_function(i,j,1,n_pol)) * ( pow(com_vec.at(c_index_function(i,j,3,n_pol)), 2.0) + pow((com_vec.at(c_index_function(i,j,4,n_pol))+(3.0/2.0) * (0.5 + i - 0.5 * n_rad) * delta_rad), 2.0) ) );
    }
  }
  
  
//     for(int i = 0; i < n_rad; i++)
//     {
//       for(int j = 0; j < n_pol; j++)
//       {
// 	sum += com_vec.at( c_index_function(i,j,1,n_pol)) * ( pow(com_vec.at(c_index_function(i,j,3,n_pol)), 2.0) + pow(com_vec.at(c_index_function(i,j,4,n_pol)), 2.0) ) ;
//       }
//     }
  
  
  sum = sum/(2.0 * n_rad * n_pol);
  
  output << time_restart+floor(time/delta_out)*delta_out << '\t' << sum << endl;
  
  output.close();
}



// FUNCTION THAT WRITES THE AVERAGED INTERNAL / THERMAL ENERGY AT EACH n_output IN FILE 



void write_averaged_internal_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol)
{
  double sum = 0.0;
  
  fstream output;
  output.open(filename.c_str(), ios::out | ios::app);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      sum += (com_vec.at( c_index_function(i,j,2,n_pol)) - 1.0/ (adia_ind * (adia_ind - 1.0)) * pow(cs_0, 3.0)/( PI * q_toomre) );
    }
  }
  
//   for(int i = 0; i < n_rad; i++)
//   {
//     for(int j = 0; j < n_pol; j++)
//     {
//       sum += com_vec.at( c_index_function(i,j,2,n_pol));
//     }
//   }
  
  
  sum = sum/(n_rad * n_pol);
  
  output << time_restart+floor(time/delta_out)*delta_out << '\t' << sum << endl;
  
  output.close();
  
}



// FUNCTION THAT WRITES THE AVERAGED GRAVITATIONAL ENERGY AT EACH n_output IN FILE 



void write_averaged_gravitational_energy(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol)
{
  double sum = 0.0;
  
  fstream output;
  output.open(filename.c_str(), ios::out | ios::app);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      sum += com_vec.at( c_index_function(i,j,0,n_pol)) * com_vec.at( c_index_function(i,j,1,n_pol));
    }
  }
  
  sum = sum/(2 *n_rad * n_pol);
  
  output << time_restart+floor(time/delta_out)*delta_out << '\t' << sum << endl;
  
  output.close();
}



// FUNCTION THAT WRITES THE SQUARED AVERAGED SURFACE DENSITY AT EACH n_output IN FILE 



void write_averaged_squared_modulus(string const filename, vector<double> & com_vec, double time, double q_toomre, double adia_ind, int n_rad, int n_pol, int k)
{
//   double sum = 0.0;
  double sum = 0.0;
  
  fstream output;
  output.open (filename.c_str(), ios::out | ios::app);
//   for(int i = 0; i < n_rad; i++)
//   {
//     for(int j = 0; j < n_pol; j++)
//     {
//       sum += com_vec.at( c_index_function(i,j,k,n_pol) );
//     }
//   }
//   
//   sum = sum / (n_rad * n_pol);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      sum += pow((com_vec.at( c_index_function(i,j,k,n_pol) ) - cs_0/( PI * q_toomre)), 2.0);
    }
  }
  
  sum = sum/(n_rad * n_pol);
  
  output << time_restart+floor(time/delta_out)*delta_out << '\t' << sum << endl;
  
  output.close();
}
 


// FUNCTION THAT WRITES THE VALUES FOR EACH GRID POINT (i,j) FOR A CERTAIN k(PHI, SIGMA, U, u, v) FOR EVERY n_output IN FILE IN GNUPLOT FORMAT


 
void write_xy_data_gnu(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k, double box_len_rad, double box_len_pol)
{
  fstream output;
  output.open (filename.c_str(), ios::out);
  for(int j = 0; j < n_pol; j++)
  {
    for(int i = 0; i < n_rad; i++)
    {
      output << com_vec.at( c_index_function(i,j,k,n_pol) ) << ' ';
    }
    output << endl;
  }
  output.close();
}



// FUNCTION THAT WRITES THE VALUES FOR EACH GRID POINT (i,j) FOR A CERTAIN k(PHI, SIGMA, U, u, v) FOR EVERY n_output IN FILE IN GENERAL FORMAT



void write_xy_data_gen(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k, double box_len_rad, double box_len_pol)
{
  fstream output;
  output.open(filename.c_str(), ios::out);
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      output << std::setprecision(16) << com_vec.at( c_index_function(i,j,k,n_pol) ) << ' '; 
    }
    output << endl;
  }
  output.close();
}



// FUNCTION THAT WRITES THE VALUES FOR EACH GRID POINT (i,j) FOR A CERTAIN k(PHI, SIGMA, U, u, v) FOR EVERY n_output IN FILE IN GENERAL FORMAT

void write_ghost_data_gen(string const filename, vector<double> & ghost_vec, int n_rad, int n_pol, int n_ghost, int k, double box_len_rad, double box_len_pol)
{
  fstream output;
  output.open(filename.c_str(), ios::out);
  for(int i = 0; i < (n_rad + 2 * n_ghost); i++)
  {
    for(int j = 0; j < (n_pol + 2 * n_ghost); j++)
    {
      output << std::setprecision(16) << ghost_vec.at( ghost_index(i,j,k,n_pol, n_ghost) ) << ' '; 
    }
    output << endl;
  }
  output.close();
}



void write_x_data_averaged_over_y(string const filename, vector<double> & com_vec, int n_rad, int n_pol, int k)
{
  double dum;
  
  fstream output;
  output.open (filename.c_str(), ios::out | ios::app);
  for(int i = 0; i < n_rad; i++)
  {
    dum = 0;
    for(int j = 0; j < n_pol; j++)
    {
      dum += com_vec.at( c_index_function(i,j,k,n_pol) ) / n_pol;
    }
    output << dum << ' ';
  }
  output << endl;
  output.close();
}



// DEFINE FUNCTION TO DETERMINE ALL K-VECTORS AND COMBINE THEM IN A MATRIX



void k_vector_components_of_grid(vector<double> & k_rad, vector<double> & k_pol, vector<double> & k_matrix, int n_rad, int n_pol, double box_len_rad, double box_len_pol, bool grid_file)
{
  // DETERMINE ALL COMPONENTS OF THE RADIAL K-VECTORS ...
  
  for(int i = 0; i < n_rad; i++)
  {
    if(i <= n_rad/2)
    {
      k_rad.at(i) = (2.0 * PI * i)/box_len_rad;
    }
    else
    {
      k_rad.at(i) = (2.0 * PI * (i - n_rad))/box_len_rad;
    }
  }
  
  // ... AND THE COMPONENTS OF THE POLOIDAL K-VECTORS ...
  
  for(int j = 0; j < n_pol; j++)
  {
    if(j <= n_poloidal/2)
    {
      k_pol.at(j) = (2.0 * PI * j)/box_len_pol;
    }
    else
    {
      k_pol.at(j) = (2.0 * PI * (j - n_pol))/box_len_pol;
    }
  }
  

  
  if(grid_file)
  {
    fstream data;
    data.open ("k_absolute.dat", ios::out);
    for(int i = 0; i < n_radial; i++)
    {
      for(int j = 0; j < n_poloidal; j++)
      {
	data <<  sqrt(pow(k_rad.at(i), 2.0) + pow(k_pol.at(j), 2.0))  << ' ';
      }
      data << endl;
    }
    data.close(); 
  }
  
}



// DEFINE FUNCTION THAT PERFORMS THE FAST FOURIER TRANSFORM OF THE SURFACE DENSITY TO SOLVE THE POISSON EQUATION, I.E. TO DETERMINE THE GRAVITATIONAL POTENTIAL FOR ALL GRID POINTS (i,j)



void fft_solve_poisson_equation(fftw_plan p_forward_transform, fftw_plan p_backward_transform, fftw_complex * in_array, fftw_complex * out_array_forward, fftw_complex * out_array_backward, vector<double> & com_vec, vector<double> & k_matrix, vector<double> & k_rad, vector<double> & k_pol, double equi_density, int n_rad, int n_pol, double time, double delta_rad, double delta_pol, double box_len_rad, double box_len_pol, int n_out, double norm, bool spectr)
{  
  // DEFINE POINTER TO INPUT AND OUTPUT ARRAYS FOR THE FAST FOURIER TRANSFORM (SEE FFTW3 MANUAL FOR NOMENCLATURE)

  // INITIALIZE INPUT DATA, CONSIDER THAT FFTW3 SOLELY TAKES PSEUDO-MULTIDIMENSIONAL ARRAYS, I.E. ARRAYS ON TOP OF ARRAYS BY USING ROW MAJOR (C++ STANDARD)
  // LINEAR INTERPOLATE VALUES ONTO A GRID (BECAUSE FFT HAS TO BE SYMMETRIC) INCLUDING A TRANSFORM OF THE COORDINATES (FORWARD TRANSFORM)
    
  double time_periodic_fft = (2.0/3.0) * (box_len_pol/box_len_rad) * floor( (3.0/2.0) * (box_len_rad/box_len_pol) * (time + time_restart) )  -  time_restart; 
  
  for(int i = 0; i < n_rad; i++ )
  {
    double shearing_fft = fabs( - (3.0/2.0) * ( 0.5 + i - 0.5 * n_rad ) * delta_rad * (time - time_periodic_fft) );
    double j_shearing_fft = shearing_fft / delta_pol;
    
    for(int j = 0; j < n_pol; j++)
    {
      if(i <= (int)floor( 0.5 * (n_rad - 1) ) )
      {
	if( (int) ceil(j_shearing_fft) + j < n_pol )
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, j + (int) floor(j_shearing_fft), 1, n_pol ) ) 
	  
					+ (j_shearing_fft - floor(j_shearing_fft)) *  com_vec.at( c_index_function( i, j + (int) ceil(j_shearing_fft), 1, n_pol))  );
	}
	else if ( ceil(j_shearing_fft) + j == n_pol )
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, n_pol - 1, 1, n_pol ) ) 
	  
					+ (j_shearing_fft - floor(j_shearing_fft)) * com_vec.at( c_index_function( i, 0, 1, n_pol ))  );
	}
	else
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, (int) floor(j_shearing_fft) 
	  
					+ j - n_pol, 1, n_pol ) ) + (j_shearing_fft - floor(j_shearing_fft)) *  com_vec.at( c_index_function( i, (int) ceil(j_shearing_fft) + j - n_pol, 1, n_pol )) );
	}
      }
      else
      {
	if(j - (int)floor(j_shearing_fft) > 0)
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, j - (int) floor(j_shearing_fft), 1, n_pol ) ) 
	   
					+ (j_shearing_fft - floor(j_shearing_fft)) *  com_vec.at( c_index_function( i, j - (int) ceil(j_shearing_fft), 1, n_pol)) );
	}
	else if(j - (int)floor(j_shearing_fft) == 0)
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, 0, 1, n_pol ) ) 
	  
					+ (j_shearing_fft - floor(j_shearing_fft)) * com_vec.at( c_index_function( i, n_pol - 1, 1, n_pol)) );
	}
	else
	{
	  in_array[j + n_pol * i][REAL] = - 2.0 * PI * ( (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * com_vec.at( c_index_function(i, j - (int) floor(j_shearing_fft) + n_pol, 1, n_pol ) )  
	  
					+ (j_shearing_fft - floor(j_shearing_fft)) *  com_vec.at( c_index_function( i, j - (int) ceil(j_shearing_fft) + n_pol, 1, n_pol)) );
	}
      }
      
      in_array[j + n_pol * i][IMAG] = 0.0;
      
      // SET OUTPUT DATA OF FORWARD TRANSFORM EQUAL ZERO
      
      out_array_forward[j + n_pol * i][REAL] = 0.0;
      out_array_forward[j + n_pol * i][IMAG] = 0.0;
      
      // SET OUTPUT DATA OF BACKWARD TRANSFORM EQUAL ZERO
      
      out_array_backward[j + n_pol * i][REAL] = 0.0;
      out_array_backward[j + n_pol * i][IMAG] = 0.0;
      
    }
  }
  
  
  
  
  
  // EXECUTE THE PLAN FOR THE FORWARD TRANSFORM, I.E. MAKE FAST FOURIER TANSFORM OF THE SURFACE DENSITY 
  
  fftw_execute(p_forward_transform);
  
  


  
  
  // GENERATE POWER SPECTRA FOR BOTH X AND Y DIRECTIONS SPEPERATELY AS WELL AS A COMPLETE 2D-SPECTRUM. 
  // THE ONE DIMENSIONAL SPECTRA ARE PRODUCED BY PROJECTION ONTO THE RESPECTIVE AXIS
  
 
  if(spectr && time - floor(time) <= time_step_old && ((int)floor(time+time_restart))%2 == 0 && time != 0.0)
  { //Notat that the spectrum-diagnostic is only called for time-points 
    //at which the shearing-periodic BC'S are periodic BC's as well. (BC = Boundary Condition)
    //Otherwise one would have to remap the output-data in a potential secondary plotting-script
    //Every second Orbit BC's are periodic in both directions
    
   
    
    
    
    // 2D-Spectrum
    stringstream filename_kxy;
    filename_kxy << "2d_spectrum_gen_" << floor(time+time_restart) << ".dat";
    ofstream output_xy;
    output_xy.open (filename_kxy.str(), ios::out);
    for(int j = 0; j < n_pol; j++)
    {
      for(int i = 0; i < n_rad; i++)
      {
	output_xy << ( pow( out_array_forward[j + n_pol * i][REAL], 2.0 ) + pow( out_array_forward[j + n_pol * i][IMAG], 2.0 ) )/(n_pol*n_rad) << ' '; 
      }
      output_xy << endl;
    }
    
    
    
    // 1D-Spectrum along kx
    stringstream filename_kx;
    filename_kx << "x_spectrum_gen_" << floor(time+time_restart) << ".dat";
    ofstream output_x;
    output_x.open (filename_kx.str(), ios::out);
    
    for(int i = 0; i < n_rad; i++)
    {
      double kx_spectrum = 0.0;
    
      for(int j = 0; j < n_pol; j++)
      {
	kx_spectrum += pow( out_array_forward[j + n_pol * i][REAL], 2.0 ) + pow( out_array_forward[j + n_pol * i][IMAG], 2.0 );
      }
     
     // Normalization is necessary as one usually expects the sum of all Fourier coefficients to be the total energy
      output_x << kx_spectrum/(n_pol*n_rad) << endl;
    }
    
    output_x.close();
   
    
    
    // 1D-Spectrum along ky
    stringstream filename_ky;
    filename_ky << "y_spectrum_gen_" << floor(time+time_restart) << ".dat";
    ofstream output_y;
    output_y.open (filename_ky.str(), ios::out);
    
    for(int j = 0; j < n_pol; j++)
    {
      double ky_spectrum = 0.0;
    
      for(int i = 0; i < n_rad; i++)
      {
	ky_spectrum += pow( out_array_forward[j + n_pol * i][REAL], 2.0 ) + pow( out_array_forward[j + n_pol * i][IMAG], 2.0 ); 
      }
      
      // Normalization is necessary as one usually expects the sum of all Fourier coefficients to be the total energy
      output_y << ky_spectrum/(n_pol*n_rad) << endl;
    }
    
    output_y.close();
  
  }
      
  
  
  
   
  
  // MULTIPLY THE OUTPUT DATA OF THE FORWARD TRANSFORM WITH THE INVERSE OF THE SQUARED ABSOLUTE OF THE K-VECTORS, THAT IS THE INVERSE K ABSOLUTE MATRIX (I. E. SOLVE THE POISSON EQUATION) AND GET THE FOURIER TRANSFORMED GRAVITATIONAL POTENTIAL
  // ONE NEEDS TO TAKE INTO ACCOUNT THE SHEARING OF WAVE-VECTORS WHEN DIVIDING BY ITS ABSOLUTE VALUE
  // THE FACTOR '1.0/sqrt(2.0)' ARISES DUE TO THE FACT THAT IN THE UNSHEARED K-GRID THE AVAILABLE SHEARING WAVE-VECTORS FORM A PARALLELOGRAM. IN CASE OF MAXIMUM SHEARING THERE APPEARS A 45 DEGREE ANGLE WITH CUT OFF AT 'sqrt(2)/2 = 1/sqrt(2)'. 
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
    
      if( sqrt( pow(( k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j) ), 2.0) + pow(k_pol.at(j), 2.0) ) < (1.0/sqrt(2.0)) * min( ( n_rad * PI / box_len_rad),( n_pol * PI / box_len_pol) ) )
      {
	out_array_forward[j + n_pol * i][REAL] = ( 1.0/sqrt(pow(( k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j) ), 2.0) + pow(k_pol.at(j), 2.0)) ) * out_array_forward[j + n_pol * i][REAL];
	out_array_forward[j + n_pol * i][IMAG] = ( 1.0/sqrt(pow(( k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j) ), 2.0) + pow(k_pol.at(j), 2.0)) ) * out_array_forward[j + n_pol * i][IMAG];
      }
      else
      {
	out_array_forward[j + n_pol * i][REAL] = 0.0;
	out_array_forward[j + n_pol * i][IMAG] = 0.0;
      }
      
    }
  }
  out_array_forward[0][REAL] = 0.0;
  out_array_forward[0][IMAG] = 0.0;
  
  
  
  
  
  // CUMULATE THE TOTAL TURBULENT GRAVITATIONAL STRESS AND WRITE THE AVERAGED VALUE INTO 'GRAVITATIONAL_STRESS.DAT'
  
  double g_stress = 0.0;
  double g_stress_no_dens = 0.0;
  
  //if(0 == count%n_out && stress)
  if(time - floor(time/delta_out)*delta_out <= time_step_old && stress && time != 0.0)
  {
    for(int i = 0; i < n_rad; i++)
    {
      for(int j = 0; j < n_pol; j++)
      {
	if( sqrt( pow(( k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j) ), 2.0) + pow(k_pol.at(j), 2.0) ) < (1.0/sqrt(2.0)) * min( ( n_rad * PI / box_len_rad),( n_pol * PI / box_len_pol) ) && i !=0 && j != 0 )
	{
	  g_stress += (1/(4*PI)) * ( pow( out_array_forward[j + n_pol * i][REAL], 2.0 ) + pow( out_array_forward[j + n_pol * i][IMAG], 2.0 ) ) * (k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j)) * k_pol.at(j) / sqrt(pow( (k_rad.at(i) + (3.0/2.0)*(time-time_periodic_fft)*k_pol.at(j) ), 2.0) + pow(k_pol.at(j), 2.0));
	  g_stress_no_dens = g_stress/com_vec.at( c_index_function(i,j,1,n_pol) );
	}
      }
    }
      
    // Note to the normalization: Above there is only one summuation appearing. But g_stress realy measures a correlation and hence the single sum originally stems from a double summation that has decayed into a single summation due to shearing periodicity.
    // But, normalization therefore needs to be done with '(n_pol*n_rad)^2' and not '(n_pol*n_rad)' in order to account for the original double summation
    g_stress /= pow( (n_pol*n_rad), 2.0 );
      
    fstream gravito_stress;
    gravito_stress.open("gravitational_stress.dat", ios::out | ios::app);
    gravito_stress << time_restart+floor(time/delta_out)*delta_out << '\t' << g_stress << '\t' << g_stress/norm << '\t' << g_stress_no_dens << endl;
    gravito_stress.close();
      
  }
  
  
  
  
  
  
  
  // EXECUTE THE PLAN FOR THE BACKWARD TRANSFORM, I.E. MAKE THE INVERSE FAST FOURIER TRANSFORM OF THE FOURIER TRANSFORMED GRAVITATIONAL POTENTIAL AND GET THE REAL-SPACE GRAVITATIONAL POTENTIAL
  
  fftw_execute(p_backward_transform);
  
  // WRITE NEW VALUES FOR THE GRAVITATIONAL POTENTIAL IN COMBINING VECTOR INCLUDING NORMALIZATION (FFTW3 PRODUCES NON-NORMALIZED OUTPUT)
  
  for(int i = 0; i < n_rad; i++)
  {
    double shearing_fft = fabs( - (3.0/2.0) * ( 0.5 + i - 0.5 * n_rad ) * delta_rad * (time - time_periodic_fft) );
    double j_shearing_fft = shearing_fft / delta_pol;
    
    for(int j = 0; j < n_pol; j++)
    {
      if(i <= (int) floor( 0.5 * (n_rad - 1) ) )
      {
	if(j - (int) floor(j_shearing_fft) < 0)
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[(j - (int) ceil(j_shearing_fft) + n_pol) + n_pol * i ][REAL] 
	    
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[(j - (int) floor(j_shearing_fft) + n_pol) + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
	else if( j - (int) floor(j_shearing_fft) == 0)
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[(n_pol - 1) + n_pol * i ][REAL] 
	  
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[0 + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
	else
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[(j - (int) ceil(j_shearing_fft)) + n_pol * i ][REAL] 
	  
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[(j - (int) floor(j_shearing_fft)) + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
      }
      else
      {
	if( j + (int) ceil(j_shearing_fft) < n_pol )
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[(j + (int) ceil(j_shearing_fft)) + n_pol * i ][REAL] 
	  
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[(j + (int) floor(j_shearing_fft)) + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
	else if( j + (int) ceil(j_shearing_fft) == n_pol )
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[0 + n_pol * i ][REAL] 
	  
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[(n_pol - 1) + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
	else
	{
	  com_vec.at( c_index_function(i,j,0,n_pol) ) = ( (j_shearing_fft - floor(j_shearing_fft)) * out_array_backward[(j + (int) ceil(j_shearing_fft) - n_pol) + n_pol * i ][REAL] 
	  
						      + (1.0 - (j_shearing_fft - floor(j_shearing_fft))) * out_array_backward[(j + (int) floor(j_shearing_fft) - n_pol) + n_pol * i][REAL] ) / (n_rad * n_pol);
	}
      }
    }
  }
  

}



// DEFINE FUNCTION THAT PERFORMS EULER SCHEME TO TIME-INTEGRATE THE GOVERNING EQUATIONS



void euler_scheme(vector<double> & com_vec, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press)
{
  
  // DECLARE ARRAY TO CONTAIN THE GRADIENT OF VIS_PRESS
  
  vector<double> vis_press_grad (2*n_rad*n_pol);
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 0 + 2 * (j + n_pol * i) ) = radial_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing);
    }
  }
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 1 + 2 * (j + n_pol * i) ) = poloidal_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, delta_pol, deri_meth);
    }
  }
  
  
  
  
  rhs_function(com_vec, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 0, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);
    
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	com_vec.at( c_index_function(i,j,k,n_pol) ) += h * k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) );
      }
    }
  }
  
}



// DEFINE FUNCTION THAT PERFORMS RUNGE KUTTA SCHEME OF 3RD ORDER TO TIME-INTEGRATE THE GOVERNING EQUATIONS



void runge_kutta_3rd_order(vector<double> & com_vec, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press)
{
  
  // DECLARE ARRAY TO CONTAIN THE GRADIENT OF VIS_PRESS
  
  vector<double> vis_press_grad (2*n_rad*n_pol);
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 0 + 2 * (j + n_pol * i) ) = radial_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing);
    }
  }
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 1 + 2 * (j + n_pol * i) ) = poloidal_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, delta_pol, deri_meth);
    }
  }
  
  
  // DECLARE ARRAY TO CACHE HALF STEPS OF RUNGE KUTTA SCHEME, THIS IS NECESSARY BECAUSE RHS_FUNCTION EXPECTS ONLY 1 ARRAY AND NOT 2 AS WOULD BE REQUIRED FOR A HALF STEP
  
  vector<double> com_vec_meta (5 * n_rad * n_pol);
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 0; k < 5; k++)
      {
	com_vec_meta.at( c_index_function(i,j,k,n_pol) ) = com_vec.at( c_index_function(i,j,k,n_pol) );
      }
    }
  }
  
  // CALL FUNCTION TO DETERMINE K1 OUT OF THE ORIGIN VALUES COM_VEC AND STORE IT IN K_STORE AT l = 0
  
  rhs_function(com_vec, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 0, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);
  
  // WRITE VALUES FOR FIRST HALF STEP ( Y + 0.5 * h * K1 ) IN CACHE-ARRAY (SEE FIRST COMMENT!)
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	com_vec_meta.at( c_index_function(i,j,k,n_pol) ) = com_vec.at( c_index_function(i,j,k,n_pol) ) + 0.5 * h *  k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) );
      }
    }
  }
  
  // CALL FUNCTION TO DETERMINE K2 OUT OF VALUES FOR FIRST HALF STEP AND STORE IT IN K_STORE AT l = 1
  
  rhs_function(com_vec_meta, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 1, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);

  // WRITE VALUES FOR SECOND STEP ( Y - h * K1 + 2 * h * K2 ) IN CACHE-ARRAY 
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	com_vec_meta.at( c_index_function(i,j,k,n_pol) ) = com_vec.at( c_index_function(i,j,k,n_pol) ) - h * k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) ) + 2.0 * h * k_store.at( k_index_function(i,j,(k-1),1,n_pol,n_rad) );
      }
    }
  }
   
  // CALL FUNCTION TO DETERMINE K3 OUT OF VALUES FOR SECOND HALF STEP AND STORE IT IN K_STORE AT l = 2
  
  rhs_function(com_vec_meta, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 2, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);
  
  // DETERMINE SUM ACCORDING TO Y_new = Y_old + 1/6 * h * ( K1 + 4 * K2 + K3)
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	com_vec.at( c_index_function(i,j,k,n_pol) ) += (1.0/6.0) * h * ( k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) ) + 4.0 * k_store.at( k_index_function(i,j,(k-1),1,n_pol,n_rad) ) + k_store.at( k_index_function(i,j,(k-1),2,n_pol,n_rad) ) );
      }
    }
  }
  
}
  


// DEFINE FUNCTION THAT PERFORMS RUNGE KUTTA SCHEME OF 4TH ORDER TO TIME-INTEGRATE THE GOVERNING EQUATIONS



void runge_kutta_4th_order(vector<double> & com_vec, vector<double> & ghost_vector, vector<double> & vis_press, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, double h, int n_rad, int n_pol, int n_ghost, double delta_rad, double delta_pol, double shearing, double v_boost, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press, double current_time)
{
  
  // DECLARE ARRAY TO CONTAIN THE GRADIENT OF VIS_PRESS. HENCE, ONE DOES NOT NEED TO CALCULATE THAT FOR ALL RUNGE KUTTA STEPS
  // THAT IS BENEFICIAL AS VIS_PRESSS IN NOT UPDATED AT EACH RK-STEP
  
  vector<double> vis_press_grad (2*n_rad*n_pol);
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 0 + 2 * (j + n_pol * i) ) = radial_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing);
    }
  }
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      vis_press_grad.at( 1 + 2 * (j + n_pol * i) ) = poloidal_1st_derivative_artificial_viscous_pressure(vis_press, prefac_list_1, i, j, n_pol, delta_pol, deri_meth);
    }
  }
 
  
  
  
  
  // FILL THE INNER PARTS OF THE GHOST VECTOR
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 0; k < 5; k++)
      {
	ghost_vector.at( ghost_index(i + n_ghost,j + n_ghost, k, n_pol, n_ghost) ) = com_vec.at( c_index_function(i,j,k,n_pol) );
      }
    }
  }
  
 
  // FILL THE REMAINING GHOST ZONES
  
  fill_the_ghost(ghost_vector, n_rad, n_pol, n_ghost, delta_rad, delta_pol, box_length_radial, box_length_poloidal, v_boost, shearing);
  
 
  
  
  // CALL FUNCTION TO DETERMINE K1 OUT OF THE ORIGIN VALUES COM_VEC AND STORE IT IN K_STORE AT l = 0
  
  rhs_function(ghost_vector, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 0, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);

  
 
  
  
  // WRITE VALUES FOR FIRST HALF STEP ( Y + 0.5 * h * K1 ) IN CACHE-ARRAY (SEE FIRST COMMENT!)
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	ghost_vector.at( ghost_index(i + n_ghost,j + n_ghost, k, n_pol, n_ghost) ) = ( com_vec.at( c_index_function(i,j,k,n_pol) ) + 0.5 * h *  k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) ) );
      }
    }
  }
  
 
  
  fill_the_ghost(ghost_vector, n_rad, n_pol, n_ghost, delta_rad, delta_pol, box_length_radial, box_length_poloidal, v_boost, shearing);
  
 
  
  // CALL FUNCTION TO DETERMINE K2 OUT OF VALUES FOR FIRST HALF STEP AND STORE IT IN K_STORE AT l = 1
  
  rhs_function(ghost_vector, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 1, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);

  
  
  // WRITE VALUES FOR SECOND HALF STEP ( Y + 0.5 * h * K2 ) IN CACHE-ARRAY 
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	ghost_vector.at( ghost_index(i + n_ghost,j + n_ghost, k, n_pol, n_ghost) ) = ( com_vec.at( c_index_function(i,j,k,n_pol) ) + 0.5 * h * k_store.at( k_index_function(i,j,(k-1),1,n_pol,n_rad) ) );
      }
    }
  }
  
  fill_the_ghost(ghost_vector, n_rad, n_pol, n_ghost, delta_rad, delta_pol, box_length_radial, box_length_poloidal, v_boost, shearing);
   
   
  // CALL FUNCTION TO DETERMINE K3 OUT OF VALUES FOR SECOND HALF STEP AND STORE IT IN K_STORE AT l = 2
  
  rhs_function(ghost_vector, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 2, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);

  
  
  // WRITE VALUES FOR THIRD STEP ( Y + h * K3 ) IN CACHE-ARRAY 

  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	ghost_vector.at( ghost_index(i + n_ghost,j + n_ghost, k, n_pol, n_ghost) ) = ( com_vec.at( c_index_function(i,j,k,n_pol) ) + h * k_store.at( k_index_function(i,j,(k-1),2,n_pol,n_rad) ) );
      }
    }
  }
  
  fill_the_ghost(ghost_vector, n_rad, n_pol, n_ghost, delta_rad, delta_pol, box_length_radial, box_length_poloidal, v_boost, shearing);
 
 
  // CALL FUNCTION TO DETERMINE K4 OUT OF VALUES FOR THIRD STEP AND STORE IT IN K_STORE AT l = 3
  
  rhs_function(ghost_vector, vis_press, vis_press_grad, k_store, prefac_list_1, prefac_list_2, prefac_list_4, n_rad, n_pol, delta_rad, delta_pol, shearing, deri_meth, inte_meth, Q, adia_ind, cooling_time, v_boost, 3, switch_compressible, switch_density_grad, switch_pressure_grad, switch_coriolis, switch_centrifugal, switch_advective, switch_cooling, switch_energy_grad, switch_gravi_grad, dam_2_den_rad, dam_2_den_pol, dam_2_ene_rad, dam_2_ene_pol, dam_2_xvel_rad, dam_2_xvel_pol, dam_2_yvel_rad, dam_2_yvel_pol, U_eq, switch_vis_press);

  
  
  // DETERMINE SUM ACCORDING TO Y_new = Y_old + 1/6 * h * ( K1 + 2 * K2 + 2 * K3 + K4)
  
  
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      for(int k = 1; k < 5; k++)
      {
	com_vec.at( c_index_function(i, j, k, n_pol) ) += (1.0/6.0) * h * ( k_store.at( k_index_function(i,j,(k-1),0,n_pol,n_rad) ) + 2.0 * k_store.at( k_index_function(i,j,(k-1),1,n_pol,n_rad) ) + 2.0 * k_store.at( k_index_function(i,j,(k-1),2,n_pol,n_rad) ) + k_store.at( k_index_function(i,j,(k-1),3,n_pol,n_rad) ) );
      }
    }
  }
  
  
  value_check_function(com_vec, n_rad, n_pol);
  
}



// DEFINE FUNCTION THAT CONTAINS THE GOVERNING EQUATIONS



void rhs_function(vector<double> & com_vec, vector<double> & vis_press, vector<double> & vis_press_grad, vector<double> & k_store, vector<double> const & prefac_list_1, vector<double> const & prefac_list_2, vector<double> const & prefac_list_4, int n_rad, int n_pol, double delta_rad, double delta_pol, double shearing, int deri_meth, int inte_meth, double Q, double adia_ind, double cooling_time, double v_boost, int l, double switch_compressible, double switch_density_grad, double switch_pressure_grad, double switch_coriolis, double switch_centrifugal, double switch_advective, double switch_cooling, double switch_energy_grad, double switch_gravi_grad, double dam_2_den_rad, double dam_2_den_pol, double dam_2_ene_rad, double dam_2_ene_pol, double dam_2_xvel_rad, double dam_2_xvel_pol, double dam_2_yvel_rad, double dam_2_yvel_pol, double U_eq, bool switch_vis_press)
{   
  
  //NOTE THAT INDICES HAVE BEEN SHIFTED BY n_ghost ACCOUNTING FOR THE EXTRA GHOST ZONES
  
  
  
  double inverse;
  
  
  for(int i = 0; i < n_rad; i++)
  {
    
    for(int j = 0; j < n_pol; j++)
    {
      
      // CALCULATE THE VELOCITY DIVERGENCE AND THE GRADIENT OF INTERNAL ENERGY ONES FOR EACH GRID POINT
      // THAT IS BASICALLY FOR OPTIMIZATION OF COMPUTING TIME 
      
      divergence = 0.0;
      thermal_grad_x = 0.0;
      thermal_grad_y = 0.0;
      
      divergence += ( radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 3, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost) + poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 4, n_pol, delta_pol, deri_meth) );
      thermal_grad_x += radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 2, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost);
      thermal_grad_y += poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 2, n_pol, delta_pol, deri_meth);
      
      //CALCULATE THE INVERSE OF THE DENSTIY (FOR PERFORMANCE REASONS)
      inverse = 1.0/com_vec.at( ghost_index(i + n_ghost,j + n_ghost,1,n_pol, n_ghost));
     
      
      for(int k = 1; k < 5; k++)
      {
	// NONDIMENSIONALIZED CONTINUITY EQUATION FOR THE SURFACE DENSITY (K == 1): - SIGMA * div u - u * grad SIGMA - DAMPING_DENSITY_X * d_x^4 * (d4_x) SIGMA - DAMPING_DENSITY_Y * d_y^4 * (d4_y) SIGMA
	
	
	
	if(k == 1)
	{
	  
	  
	  k_store.at( k_index_function(i,j,0,l,n_pol,n_rad) ) = (
	  
	  // - SIGMA * ( (d_x) u + (d_y) v ) 
	  
	  - switch_compressible * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,1,n_pol, n_ghost) ) * divergence 
	  
	  // - u * (d_x) SIGMA
	  
	  - switch_density_grad * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,3,n_pol, n_ghost) ) * radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 1, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost) 
	  
	  // - v * (d_y) SIGMA
	  
	  - switch_density_grad * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,4,n_pol, n_ghost) ) * poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 1, n_pol, delta_pol, deri_meth)
	  
	  // + DAMPING_2_DENSITY_X * d_x^2 * (d2_x) SIGMA
	  
	  + dam_2_den_rad * pow(delta_rad, 1.0) * radial_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 1, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // + DAMPING_2_DENSITY_Y * d_y^2 * (d2_y) SIGMA
	  
	  + dam_2_den_pol * pow(delta_pol, 1.0) * poloidal_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 1, n_pol, delta_pol, deri_meth));
	  
	  // - DAMPING_4_DENSITY_X * d_x^4 * (d4_x) SIGMA
	  
	  //- dam_4_den_rad * pow(delta_rad, 3.0) * radial_4th_derivative(com_vec, prefac_list_4, i, j, 1, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // - DAMPING_4_DENSITY_Y * d_y^4 * (d4_y) SIGMA
	  
	  //- dam_4_den_pol * pow(delta_pol, 3.0) * poloidal_4th_derivative(com_vec, prefac_list_4, i, j, 1, n_pol, delta_pol, deri_meth) );
	} 
	
	// NONDIMENSIONALIZED TRANSPORT EQUATION FOR THE INTERNAL ENERGY (K == 2): - gamma * U * div u - U/tau_cooling - u * grad U - DAMPING_ENERGY_X * d_x^4 * (d4_x) U - DAMPING_ENERGY_Y * d_y^4 * (d4_y) U
	
	
	
	else if(k == 2)
	{
	  
	  k_store.at( k_index_function(i,j,1,l,n_pol,n_rad) ) = (
	  
	  // - gamma * U * ( (d_x) u + (d_y) v ) 
	  
	  - switch_compressible * adia_ind * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,2,n_pol, n_ghost) ) * divergence 
	  
	  // - (U - U_eq - cooling_mode*cs0^2*delta\Sigma / (gamma * (gamma - 1)))/tau_cooling
	  // cooling_mode is a double variable that should be chosen in the interval [0, 1]. 
	  
	  - switch_cooling * (( com_vec.at( ghost_index(i + n_ghost,j + n_ghost,2,n_pol, n_ghost) ) - U_eq 
				- cooling_mode*cs_0*cs_0*(com_vec.at( ghost_index(i + n_ghost,j + n_ghost,1,n_pol, n_ghost) ) - cs_0/(PI*q_toomre))/(adia_ind*(adia_ind-1.0)) ) / cooling_time )
	  
	  // - u * (d_x) U
	  
	  - switch_energy_grad * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,3,n_pol, n_ghost) ) * thermal_grad_x 
	  
	  // - v * (d_y) U
	  
	  - switch_energy_grad * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,4,n_pol, n_ghost) ) * thermal_grad_y
	  
	  // + DAMPING_2_ENERGY_X * d_x^2 * (d2_x) U
	  
	  + dam_2_ene_rad * pow(delta_rad, 1.0) * radial_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 2, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // + DAMPING_2_ENERGY_Y * d_y^2 * (d2_y) U
	  
	  + dam_2_ene_pol * pow(delta_pol, 1.0) * poloidal_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 2, n_pol, delta_pol, deri_meth));
	  
	  // - DAMPING_4_ENERGY_X * d_x^4 * (d4_x) U
	  
	  //- dam_4_ene_rad * pow(delta_rad, 3.0) * radial_4th_derivative(com_vec, prefac_list_4, i, j, 2, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // - DAMPING_4_ENERGY_Y * d_y^4 * (d4_y) U
	  
	  //- dam_4_ene_pol * pow(delta_pol, 3.0) * poloidal_4th_derivative(com_vec, prefac_list_4, i, j, 2, n_pol, delta_pol, deri_meth) );
	  
	  // IN CASE, THAT THE GAS IS COMPRESSED: ADDITIONAL TERM (- art_vis_press * ( (d_x) u + (d_y) v ) )
	  
	  if (switch_vis_press && divergence < 0.0)
	  {
	    k_store.at( k_index_function(i,j,(k-1), l, n_pol, n_rad) ) -= vis_press.at(j + n_pol * i) * divergence;
	  }
	}
	
	// NONDIMENSIONALIZED EULER EQUATION FOR THE VELOCITY FIELD IN RADIAL (x) DIRECTION (K == 3): - (gamma - 1) * ( (d_x) U ) / SIGMA + 2 * v + 3 * x - (d_x) PHI - u * (d_x) u - v * (d_y) u - DAMPING_XVELOCITY_X * d_x^4 * (d4_x) u - DAMPING_XVELOCITY_Y * d_y^4 * (d4_y) u
	

	
	else if(k == 3)
	{
	  
	  k_store.at( k_index_function(i,j,2,l,n_pol,n_rad) ) = (
	  
	  // - (gamma - 1) * (d_x) U / (SIGMA)
	  
	  - switch_pressure_grad * (adia_ind - 1.0) * thermal_grad_x * inverse 
	  
	  // + 2.0 * v
	  
	  + switch_coriolis * 2.0 * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,4,n_pol, n_ghost) ) 
	  
	  // + 3.0 * x
	  
	  + switch_centrifugal * 3.0 * (0.5 + i - 0.5 * n_rad) * delta_rad 
	  
	  // - (d_x) PHI
	  
	  - switch_gravi_grad * radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 0, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost) 
	  
	  // - u * (d_x) u
	  
	  - switch_advective * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,3,n_pol, n_ghost) ) * radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 3, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost) 
	  
	  // - v * (d_y) u
	  
	  - switch_advective * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,4,n_pol, n_ghost) ) * poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 3, n_pol, delta_pol, deri_meth)
	  
	  // + DAMPING_2_XVELOCITY_X * d_x^2 * (d2_x) u
	  
	  + dam_2_xvel_rad * pow(delta_rad, 1.0) * radial_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 3, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // + DAMPING_2_XVELOCITY_Y * d_y^2 * (d2_y) u
	  
	  + dam_2_xvel_pol * pow(delta_pol, 1.0) * poloidal_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 3, n_pol, delta_pol, deri_meth));
	  
	  // - DAMPING_4_XVELOCITY_X * d_x^4 * (d4_x) u
	  
	  //- dam_4_xvel_rad * pow(delta_rad, 3.0) * radial_4th_derivative(com_vec, prefac_list_4, i, j, 3, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // - DAMPING_4_XVELOCITY_Y * d_y^4 * (d4_y) u
	  
	  //- dam_4_xvel_pol * pow(delta_pol, 3.0) * poloidal_4th_derivative(com_vec, prefac_list_4, i, j, 3, n_pol, delta_pol, deri_meth) );
	  
	  // IN CASE THAT THE GAS IS COMPRESSED: ADDITIONAL TERM - (d_x) art_vis_press / (SIGMA)
	  
	  if (switch_vis_press && divergence < 0.0)
	  {
	    k_store.at( k_index_function(i,j,(k-1), l, n_pol, n_rad) ) -= ( vis_press_grad.at(0 + 2 * (j + n_pol * i)) * inverse );
	  }
	}
      
	
	// NONDIMENSIONALIZED EULER EQUATION FOR THE VELOCITY FIELD IN POLOIDAL (y) DIRECTION (K == 4): - (gamma - 1) * ( (d_y) U ) / SIGMA - 2 * u - (d_y) PHI - u * (d_x) v - v * (d_y) v - DAMPING_YVELOCITY_X * d_x^4 * (d4_x) v - DAMPING_YVELOCITY_Y * d_y^4 * (d4_y) v
	
	
	
	else
	{
	  
	  k_store.at( k_index_function(i,j,3,l,n_pol,n_rad) ) = (
	  
	  // - (gamma - 1) * ( (d_y) U )/ SIGMA
	  
	  - switch_pressure_grad * (adia_ind - 1.0) * thermal_grad_y * inverse
	  
	  // - 2.0 * u
	  
	  - switch_coriolis * 2.0 * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,3,n_pol, n_ghost) ) 
	  
	  // - (d_y) PHI
	  
	  - switch_gravi_grad * poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 0, n_pol, delta_pol, deri_meth) 
	  
	  // - u * (d_x) v
	  
	  - switch_advective * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,3,n_pol, n_ghost) ) * radial_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 4, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost) 
	  
	  // - v * (d_y) v 
	  
	  - switch_advective * com_vec.at( ghost_index(i + n_ghost,j + n_ghost,k,n_pol, n_ghost) ) * poloidal_1st_derivative(com_vec, prefac_list_1, i + n_ghost, j + n_ghost, 4, n_pol, delta_pol, deri_meth)
	  
	  // + DAMPING_2_YVELOCITY_X * d_x^2 * (d2_x) v
	  
	  + dam_2_yvel_rad * pow(delta_rad, 1.0) * radial_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 4, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // + DAMPING_2_YVELOCITY_Y * d_y^2 * (d2_y) v
	  
	  + dam_2_yvel_pol * pow(delta_pol, 1.0) * poloidal_2nd_derivative(com_vec, prefac_list_2, i + n_ghost, j + n_ghost, 4, n_pol, delta_pol, deri_meth));
	  
	  // - DAMPING_4_YVELOCITY_X * d_x^4 * (d4_x) v
	  
	  //- dam_4_yvel_rad * pow(delta_rad, 3.0) * radial_4th_derivative(com_vec, prefac_list_4, i, j, 4, n_pol, n_rad, delta_rad, delta_pol, deri_meth, shearing, v_boost)
	  
	  // - DAMPING_4_YVELOCITY_Y * d_y^4 * (d4_y) v
	  
	  //- dam_4_yvel_pol * pow(delta_pol, 3.0) * poloidal_4th_derivative(com_vec, prefac_list_4, i, j, 4, n_pol, delta_pol, deri_meth) );
	  
	  // IN CASE THAT THE GAS IS COMPRESSED: ADDITIONAL TERM - (d_y) art_vis_press / (SIGMA)
	  
	  if (switch_vis_press && divergence < 0.0)
	  {
	    k_store.at( k_index_function(i,j,(k-1), l, n_pol, n_rad) ) -= ( vis_press_grad.at(1 + 2 * (j + n_pol * i)) * inverse ) ;
	  }
	}
      }
     
    }
  }
  
}



// DEFINE INDEXFUNCTIONS FOR COMBINING VECTOR, GHOST-VECTOR AS WELL AS K STORE MATRIX: PASS GRIDPOINT (i,j), THE QUANTITY (k) AS WELL AS THE POLOIDAL GRID SIZE (n_pol) AND RETURN THE ACCORDING INDEX OF THE ARRAY



int c_index_function(int i, int j, int k, int n_pol)
{
  return (k + 5 * (j + n_pol * i));
}

int ghost_index(int i, int j, int k, int n_pol, int n_ghost)
{
  return (k + 5 * (j + (n_pol + 2 * n_ghost) * i));
}


int k_index_function(int i, int j, int k, int l, int n_pol, int n_rad)
{
  return ( k + 4 * (j + n_pol * (i + n_rad * l)) );
}



// DEFINE FUNCTIONS THAT PERFORMS THE 1ST-ORDER SPATIAL DERIVATIVES (RADIAL AND POLOIDAL DIRECTION) CONSIDERING THE ORDER OF THE CENTRAL DIFFERENCES INCLUDING LINEAR INTERPOLATION BETWEEN TWO GRID POINTS



double radial_1st_derivative(vector<double> & com_vec, vector<double> const & prefac_list_1, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost)
{
  // DEFINE VARIABLE TO STORE ALL VALUES REQUIRED FOR THE RADIAL (x) DERIVATIVE
  
  double sum_rad_der_1 = 0.0;
 
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRID POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_i = - deri_meth; delta_i <= deri_meth; delta_i++)
  {
    sum_rad_der_1 += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i + delta_i, j, k, n_pol, n_ghost) );
  }
   
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (x) DIRECTION
    
  return sum_rad_der_1 /= delta_rad;
}



double poloidal_1st_derivative(vector<double> & com_vec, vector<double> const & prefac_list_1, int i, int j, int k, int n_pol, double delta_pol, int deri_meth)
{
  // DEFINE VALUE TO STORE ALL REQUIRED VALUES REQUIRED FOR DERIVATIVE IN POLOIDAL (y) DIRECTION
  
  double sum_pol_der_1 = 0.0;																		
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_j = - deri_meth; delta_j <= deri_meth; delta_j++)
  {
    sum_pol_der_1 += prefac_list_1.at( delta_j + 4 + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i,j + delta_j,k,n_pol, n_ghost) );
  }
  
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (y) DIRECTION
  
  return sum_pol_der_1 /= delta_pol;																	
}



// DEFINE FUNCTIONS THAT PERFORMS THE 2ND-ORDER SPATIAL DERIVATIVES (RADIAL AND POLOIDAL DIRECTION) CONSIDERING THE ORDER OF THE CENTRAL DIFFERENCES INCLUDING LINEAR INTERPOLATION BETWEEN TWO GRID POINTS



double radial_2nd_derivative(vector<double> & com_vec, vector<double> const & prefac_list_2, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost)
{
  // DEFINE VALUE TO STORE ALL VALUES REQUIRED FOR THE RADIAL (x) DERIVATIVE
  
  double sum_rad_der_2 = 0.0;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_i = - deri_meth; delta_i <= deri_meth; delta_i++)
  { 
    sum_rad_der_2 += prefac_list_2.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i + delta_i, j, k, n_pol, n_ghost) ); 
  }
   
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (x) DIRECTION
    
  return sum_rad_der_2 /= pow(delta_rad, 2.0);
}



double poloidal_2nd_derivative(vector<double> & com_vec, vector<double> const & prefac_list_2, int i, int j, int k, int n_pol, double delta_pol, int deri_meth)
{
  // DEFINE VALUE TO STORE ALL REQUIRED VALUES REQUIRED FOR DERIVATIVE IN POLOIDAL (y) DIRECTION
  
  double sum_pol_der_2 = 0.0;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_j = - (deri_meth + 1); delta_j <= (deri_meth + 1); delta_j++)
  {
    sum_pol_der_2 += prefac_list_2.at( delta_j + 4 + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i,j + delta_j,k,n_pol, n_ghost) );
  }
  
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (y) DIRECTION
  
  return sum_pol_der_2 /= pow(delta_pol, 2.0);	
}



// DEFINE FUNCTIONS THAT PERFORMS THE 4TH-ORDER SPATIAL DERIVATIVES (RADIAL AND POLOIDAL DIRECTION) CONSIDERING THE ORDER OF THE CENTRAL DIFFERENCES INCLUDING LINEAR INTERPOLATION BETWEEN TWO GRID POINTS



double radial_4th_derivative(vector<double> & com_vec, vector<double> const & prefac_list_4, int i, int j, int k, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing, double v_boost)
{
  // DEFINE VALUE TO STORE ALL VALUES REQUIRED FOR THE RADIAL (x) DERIVATIVE
  
  double sum_rad_der_4 = 0.0;
  
  // DETERMINE NUMBER OF SHEARED GRID POINTS IN POLOIDAL (y) DIRECTION
  
  double j_shearing = shearing/delta_pol;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_i = - (deri_meth + 1); delta_i <= (deri_meth + 1); delta_i++)
  {
    // CHECK WHETHER A REQUIRED GRID POINT IS BEFORE THE INNER BOUNDARY (RADIAL (x) DIRECTION) OF THE GRID
    
    if((i + delta_i) < 0 )
    {
      if( (j - (int) floor(j_shearing)) > 0 )
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i + n_rad, j - (int) floor(j_shearing), k, n_pol) ) 
	
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function( i + delta_i + n_rad, j - (int) ceil(j_shearing), k, n_pol ) ) );
      }
      else if( j - (int) floor(j_shearing) == 0 )
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i + n_rad, 0, k, n_pol) ) 
	  
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function(i + delta_i + n_rad, n_pol - 1, k, n_pol) ) );
      }
      else
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i + n_rad, j - (int) floor(j_shearing) + n_pol, k, n_pol) )
	
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function(i + delta_i + n_rad, j - (int) ceil(j_shearing) + n_pol, k, n_pol) ) );
      }
      if(k == 4)
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * v_boost;
      }
    }
    else if( (i + delta_i) > (n_rad - 1) )
    {
      if(j + (int) ceil(j_shearing) < n_pol)
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i - n_rad, j + (int) floor(j_shearing), k, n_pol) )
	  
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function(i + delta_i - n_rad, j + (int) ceil(j_shearing), k, n_pol) ) );
      }
      else if( j + (int) ceil(j_shearing) == n_pol )
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i - n_rad, n_pol - 1, k, n_pol) )
	
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function(i + delta_i - n_rad, 0, k, n_pol) ) );
      }
      else
      {
	sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * com_vec.at( c_index_function(i + delta_i - n_rad, j + (int) floor(j_shearing) - n_pol, k, n_pol) )
	
		      + (j_shearing - floor(j_shearing)) * com_vec.at( c_index_function(i + delta_i - n_rad, j + (int) ceil(j_shearing) - n_pol, k, n_pol) ) );
      }
      if(k == 4)
      {
	sum_rad_der_4 -= prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * v_boost;
      }
    }
    else
    {
       sum_rad_der_4 += prefac_list_4.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i + delta_i, j, k, n_pol, n_ghost) );
    } 
  }
   
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (x) DIRECTION
    
  return sum_rad_der_4 /= pow(delta_rad, 4.0);
}



double poloidal_4th_derivative(vector<double> & com_vec, vector<double> const & prefac_list_4, int i, int j, int k, int n_pol, double delta_pol, int deri_meth)
{
  // DEFINE VALUE TO STORE ALL REQUIRED VALUES REQUIRED FOR DERIVATIVE IN POLOIDAL (y) DIRECTION
  
  double sum_pol_der_4 = 0.0;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_j = - (deri_meth + 1); delta_j <= (deri_meth + 1); delta_j++)
  {
    // CHECK WHETHER A REQUIRED GRID POINT IS OUTSIDE THE LEFT BOUNDARY (POLODIAL (y) DIRECTION) OF THE GRID
    
    if((j + delta_j) < 0)																	
    {
      sum_pol_der_4 += prefac_list_4.at( delta_j + 4 + 9 * (deri_meth - 1) ) * com_vec.at( c_index_function(i,j + delta_j + n_pol,k,n_pol) );
    }
  
    // CHECK WHETHER A REQUIRED GRID POINT IS OUTSIDE THE RIGHT BOUNDARY (POLODIAL (y) DIRECTION) OF THE GRID
    
    else if( (j + delta_j) > (n_pol - 1) )															
    {
      sum_pol_der_4 += prefac_list_4.at( delta_j + 4 + 9 * (deri_meth - 1) ) * com_vec.at( c_index_function(i,j + delta_j - n_pol,k,n_pol) );
    }
    
    // IN CASE THE TWO CHECKS ABOVE WERE WRONG THE REQUIRED GRID POINT MUST BE WITHIN THE GRID
    
    else																			
    {
      sum_pol_der_4 += prefac_list_4.at( delta_j + 4 + 9 * (deri_meth - 1) ) * com_vec.at( ghost_index(i,j + delta_j,k,n_pol, n_ghost) );
    }
  }
  
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (y) DIRECTION
  
  return sum_pol_der_4 /= pow(delta_pol, 4.0);					
}



// DEFINE FUNCTIONS THAT PERFORM THE 1ST-ORDER SPATIAL DERIVATIVES (RADIAL AND POLOIDAL DIRECTION) FOR THE ARTIFICIAL VISCOUS PRESSURE, CONSIDERING THE ORDER OF THE CENTRAL DIFFERENCES INCLUDING LINEAR INTERPOLATION BETWEEN TWO GRID POINTS



double radial_1st_derivative_artificial_viscous_pressure(vector<double> & vis_press, vector<double> const & prefac_list_1, int i, int j, int n_pol, int n_rad, double delta_rad, double delta_pol, int deri_meth, double shearing)
{
  // DEFINE VARIABLE TO STORE ALL VALUES REQUIRED FOR THE RADIAL (x) DERIVATIVE
  
  double sum_rad_der_1_art_vis_press = 0.0;
  
  // DETERMINE NUMBER OF SHEARED GRID POINTS IN POLOIDAL (y) DIRECTION
  
  double j_shearing = shearing/delta_pol;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRID POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_i = - deri_meth; delta_i <= deri_meth; delta_i++)
  {
    // CHECK WHETHER A REQUIRED GRID POINT IS SITUATED BEFORE THE INNER BOUNDARY (RADIAL (x) DIRECTION) OF THE GRID
    
    if((i + delta_i) < 0 )
    {
      if( (j - (int) floor(j_shearing)) > 0 )
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( (j - (int) floor(j_shearing)) + n_pol * (i + delta_i + n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( (j - (int) ceil(j_shearing)) + n_pol * (i + delta_i + n_rad) ) );
      }
      else if( j - (int) floor(j_shearing) == 0 )
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( n_pol * (i + delta_i + n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( (n_pol - 1) + n_pol * (i + delta_i + n_rad) ) );
      }
      else
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( (j - (int) floor(j_shearing) + n_pol) + n_pol * (i + delta_i + n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( (j - (int) ceil(j_shearing) + n_pol) + n_pol * (i + delta_i + n_rad ) ) );
      }
    }
    else if( (i + delta_i) > (n_rad - 1) )
    {
      if(j + (int) ceil(j_shearing) < n_pol)
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( (j + (int) floor(j_shearing)) + n_pol * (i + delta_i - n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( (j + (int) ceil(j_shearing) ) + n_pol * (i + delta_i - n_rad) ) );
      }
      else if( j + (int) ceil(j_shearing) == n_pol )
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( (n_pol - 1) + n_pol * (i + delta_i - n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( 0 +  n_pol * (i + delta_i - n_rad) ) );
      }
      else
      {
	sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1)) * ( (1.0 - (j_shearing - floor(j_shearing))) * vis_press.at( (j + (int) floor(j_shearing) - n_pol) + n_pol * (i + delta_i - n_rad) )
	
				    + (j_shearing - floor(j_shearing)) * vis_press.at( (j + (int) ceil(j_shearing) - n_pol ) + n_pol * (i + delta_i - n_rad) ) );
      }
    }
    else
    {
      sum_rad_der_1_art_vis_press += prefac_list_1.at( (delta_i + 4) + 9 * (deri_meth - 1) ) * vis_press.at(j + n_pol * (i + delta_i));
    } 
  }
   
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (x) DIRECTION
    
  return sum_rad_der_1_art_vis_press /= delta_rad;
  
  
  
}



double poloidal_1st_derivative_artificial_viscous_pressure(vector<double> & vis_press, vector<double> const & prefac_list_1, int i, int j, int n_pol, double delta_pol, int deri_meth)
{
  // DEFINE VALUE TO STORE ALL REQUIRED VALUES REQUIRED FOR DERIVATIVE IN POLOIDAL (y) DIRECTION
  
  double sum_pol_der_1_art_vis_press = 0.0;
  
  // LOOP CONSIDERS NUMBER OF REQUIRED ADJACENT GRIP POINTS DEPENDING ON THE CHOSEN DERIVATIVE METHOD
  
  for(int delta_j = - (deri_meth + 1); delta_j <= (deri_meth + 1); delta_j++)
  {
    // CHECK WHETHER A REQUIRED GRID POINT IS OUTSIDE THE LEFT BOUNDARY (POLODIAL (y) DIRECTION) OF THE GRID
    
    if((j + delta_j) < 0)																	
    {
      sum_pol_der_1_art_vis_press += prefac_list_1.at( delta_j + 4 + 9 * (deri_meth - 1)) * vis_press.at( (j + delta_j + n_pol) + n_pol * i );
    }
  
    // CHECK WHETHER A REQUIRED GRID POINT IS OUTSIDE THE RIGHT BOUNDARY (POLODIAL (y) DIRECTION) OF THE GRID
    
    else if( (j + delta_j) > (n_pol - 1) )															
    {
      sum_pol_der_1_art_vis_press += prefac_list_1.at( delta_j + 4 + 9 * (deri_meth - 1)) * vis_press.at( (j + delta_j - n_pol) + n_pol * i );
    }
    
    // IN CASE THE TWO CHECKS ABOVE WERE WRONG THE REQUIRED GRID POINT MUST BE WITHIN THE GRID
    
    else																			
    {
      sum_pol_der_1_art_vis_press += prefac_list_1.at( delta_j + 4 + 9 * (deri_meth - 1)) * vis_press.at( (j + delta_j) + n_pol * i );
    }
  }
  
  // DIVIDE ACCUMULATED VALUE BY SIZE OF FINITE DIFFERENCE STEP IN RADIAL (y) DIRECTION
  
  return sum_pol_der_1_art_vis_press /= delta_pol;			
}



// DEFINE FUNCTION THAT CHECKS IF THE SURFACE DENSITY OR THE INTERNAL ENERGY DROPS BELOW ZERO IN EACH TIME STEP. IN THIS CASE, THE ACCORDING VALUE IS SET TO A SMALL, BUT POSITIVE VALUE


void value_check_function(vector<double> & com_vec, int n_rad, int n_pol)
{
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_pol; j++)
    {
      if( com_vec.at( c_index_function(i,j,1,n_pol) )  <= 1e-8)
      {
	com_vec.at( c_index_function(i,j,1,n_pol) ) = 1e-8;
      }
      
      if( com_vec.at( c_index_function(i,j,2,n_pol) )  <= 1e-8)
      {
	com_vec.at( c_index_function(i,j,2,n_pol) ) = 1e-8;
      }
      
    }
  }
}




// FUNCTION THAT FILLS THE GHOST ZONES WITH INTERPOLATED VALUES


void fill_the_ghost(vector<double> & ghost_vec, int n_rad, int n_pol, int n_ghost, double delta_rad, double delta_pol, double box_len_rad, double box_len_pol, double v_boost, double shearing)
{  
 
  //FILL ALL THE GHOST ZONES OF GHOST_VEC(...) FOR PHI, SIGMA, U AND vx, vy
  //NOTE THAT Vy CHANGES IN THE WAY THAT ONE HAS TO AD A BOOST OF MAGNITUDE 3/2 * Omega * Lx IN ORDER TO ACCOUNT FOR THE BACKGROUND SHEAR FLOW

  
  
  
  //CALCULATE THE MOMENTARY SHEARING RATE (NECESSARY FOR PERFORMING THE INTERPOLATION)
  double j_shear = shearing / delta_pol;
  
  //VECTOR CONTAINING THE BOOST IN Y-DIRECTION
  //THAT BOOST IS NECESSARY BECAUSE THE MAXIMUMG SHEAR VELOCITY 1.5*L HAS TO BE ADDED TO THE Y-VELOCITY IN ORDER TO GUARANTEE SHEARING PERIODICITY 
  //ALL OTHER QUANTITIES DO NOT REQUIRE A BOOST
  vector<double> boost {0.0, 0.0, 0.0, 0.0, v_boost};
  
  
  
  
  //FILL THE UPPER AND LOWER GHOST ZONES IN POLOIDAL DIRECTION
  for (int k = 0; k < 5; k++)
  {
    
      for(int i = 0; i < n_rad; i++ )
      {
	  for(int j = 0; j < n_ghost; j++)
	  {
		  ghost_vec.at( ghost_index(i + n_ghost, j, k, n_pol, n_ghost) ) = ghost_vec.at( ghost_index(i + n_ghost, j + n_pol, k, n_pol, n_ghost) );
		  ghost_vec.at( ghost_index(i + n_ghost, j + n_ghost + n_pol, k, n_pol, n_ghost) ) = ghost_vec.at( ghost_index(i + n_ghost, j + n_ghost, k, n_pol, n_ghost) );
	  }
      }
  
   

  
  
	  //FILL THE LEFT GHOST ZONES IN THE NEGATIVE RADIAL DIRECTION
	  for(int i = 0; i < n_ghost; i++ )
	  {
		  
		  for(int j = 0; j < n_ghost + (int)floor(j_shear); j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = 
						(1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i + n_rad, n_pol + j - (int)floor(j_shear), k, n_pol, n_ghost) )
					  + (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i + n_rad, n_pol + j - (int)ceil(j_shear), k, n_pol, n_ghost) )
					  + boost.at(k);
		  }
		  
		  for(int j = n_ghost + (int)floor(j_shear); j < n_ghost + (int)floor(j_shear) + 1; j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = 
						(1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i + n_rad, n_ghost, k, n_pol, n_ghost) )
					  + (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i + n_rad, n_ghost + n_pol - 1, k, n_pol, n_ghost) )
					  + boost.at(k);
		  }
		  
		  for(int j = n_ghost + (int)floor(j_shear) + 1; j < n_pol + 2 * n_ghost; j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = 
						(1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i + n_rad, j - (int)floor(j_shear), k, n_pol, n_ghost) )
					  + (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i + n_rad, j - (int)ceil(j_shear), k, n_pol, n_ghost) )
					  + boost.at(k);					  
		  }
		  
	  }
  
    

	  //FILL THE RIGHT GHOST ZONES IN THE POSITIVE RADIAL DIRECTION
	  /*for(int i = n_ghost + n_rad; i < n_rad + 2 * n_ghost; i++ )
	  {
		  
		  for(int j = 0; j < n_ghost + n_pol - 1 - (int)ceil(j_shear); j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i - n_rad, 1 + (int)ceil(j_shear) + j, k, n_pol, n_ghost) )
							  + (1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i - n_rad, 1 + (int)floor(j_shear) + j, k, n_pol, n_ghost) )
							  - boost.at(k);
		  }
		  for(int j = n_ghost + n_pol - 1 - (int)ceil(j_shear); j < n_ghost + n_pol - (int)ceil(j_shear); j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = (1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i - n_rad, n_ghost + n_pol - 1, k, n_pol, n_ghost) )
							  + (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i - n_rad, n_ghost, k, n_pol, n_ghost) )
							  - boost.at(k);
		  }
		  for(int j = n_ghost + n_pol - (int)ceil(j_shear); j < n_pol + 2 * n_ghost; j++)
		  {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) = (1.0 - (j_shear - floor(j_shear))) * ghost_vec.at( ghost_index(i - n_rad, j - n_pol + 1 + (int)floor(j_shear), k, n_pol, n_ghost) )
							  + (j_shear - floor(j_shear)) * ghost_vec.at( ghost_index(i - n_rad, j - n_pol + 1 + (int)ceil(j_shear), k, n_pol, n_ghost) )
							  - boost.at(k);
		  }
	  }*/
	  
	  
	 
	  //FILL THE RIGHT GHOST ZONES IN THE POSITIVE RADIAL DIRECTION
	  for(int i = n_rad + n_ghost; i < n_rad + 2 * n_ghost; i++){                                              

	      for(int j = 0; j < n_ghost + n_pol - (int)ceil(j_shear); j++) {                                    
		      ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) =
				  (1.0 - (j_shear - floor(j_shear))) *  ghost_vec.at( ghost_index(i - n_rad, (int)floor(j_shear) + j, k, n_pol, n_ghost) )
				  + (j_shear - floor(j_shear)) *        ghost_vec.at( ghost_index(i - n_rad, (int)ceil(j_shear) + j, k, n_pol, n_ghost) )
				  - boost.at(k);

		  }

		  for(int j = n_ghost + n_pol - (int)ceil(j_shear); j < n_ghost + n_pol - (int)ceil(j_shear) + 1; j++) {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) =
				  (1.0 - (j_shear - floor(j_shear))) *  ghost_vec.at( ghost_index(i - n_rad, n_ghost + n_pol - 1, k, n_pol, n_ghost) )
				  + (j_shear - floor(j_shear)) *        ghost_vec.at( ghost_index(i - n_rad, n_ghost, k, n_pol, n_ghost) )
				  - boost.at(k);

		  }

		  for(int j = n_ghost + n_pol - (int)ceil(j_shear) + 1; j < n_pol + 2 * n_ghost; j++) {
			  ghost_vec.at( ghost_index(i, j, k, n_pol, n_ghost) ) =
				  (1.0 - (j_shear - floor(j_shear))) *  ghost_vec.at( ghost_index(i - n_rad, j - n_pol + (int)floor(j_shear), k, n_pol, n_ghost) )
				  + (j_shear - floor(j_shear)) *        ghost_vec.at( ghost_index(i - n_rad, j - n_pol + (int)ceil(j_shear), k, n_pol, n_ghost) )
				  - boost.at(k);

		  }

	  }
	  
  
    
  }
  
}

  
  
  
  
 



