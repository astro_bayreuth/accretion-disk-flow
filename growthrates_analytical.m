clear all

################################################################################
#SHORT DOCUMENTATION
#The program calculates the growthrate (g), the wave vector (k) and the wave 
#length (lambda) of the fastest growing mode depending on the following 
#input-parameters:
#-> Toomre parameter Q ("Q_Toomre" in input_diskFlow.json),
#-> Cooling-Switch ("Switch_cooling" in input_diskFlow.json),
#-> Cooling time scale beta ("Cooling_time" in input_diskFlow.json).
#The Toomre Q is dimensionless by default and beta = \tau_c*\Omega_0 is the
#cooling time in code units.
#Depending on the version of DiskFlow that is used, there also is the option
#"Cooling_mode" in input_diskFlow.json. The restults of this program only hoold 
#for "Cooling_mode = 0.0". 
#The required input-parameters can be typed in below.
################################################################################



#SET INPUT PARAMETERS
Q = 1.0;			#Toomre parameter
cooling = true;		#Decides whether cooling is used or not
beta = 10;			#Cooling time






#THE FOLLOWING BLOCK APPLIES THE ANALYTICAL EXPRESSIONS AND 
#DISPLAYS THE CALCULATED RESULTS ON THE SCREEN.
if (cooling == (true))		#CASE WITH COOLING ON
	
	growthrate = -2^(1/3)*(3-3/Q^2)/( 3*( 27*(1/beta)/(Q^2) + sqrt(4*(3-3/Q^2)^3 + 729*(1/beta)^2/(Q^4)) )^(1/3) )  ...
	              +  (1/(3*2^(1/3)))*( 27*(1/beta)/(Q^2) + sqrt(4*(3-3/Q^2)^3 + 729*(1/beta)^2/(Q^4)) )^(1/3);
	kmax = ( 1 + growthrate * beta )/( Q * growthrate * beta );
	lambda = 2*pi/kmax;
	
	#DISPLAY THE RESULTS ON THE SCREEN
	disp(" ");
	disp("Shown are the growthrate (g), the wave vector (k) and the wave length (lambda) of the fastest growing mode.");
	disp(" ");
	#DISPLAY THE GROWTHRATE OF THE FASTEST GROWING MODE
	disp ("The growthrate g is "), disp (growthrate);
	disp(" ");
	#DISPLAY THE WAVE VECTOR OF THE FASTEST GROWING MODE
	disp ("The corresponding wave vector k is "), disp (kmax);
	disp(" ");
	#DISPLAY THE WAVE LENGTH OF THE FASTEST GROWING MODE
	disp ("The wave length lambda is "), disp (lambda);
	
else	#CASE WITH COOLING OFF (CLASSICAL TOOMRE)
	
	if (Q > 1.0)
	
		disp(" ");
		disp("Attention! Q > 1 and Cooling is off  -->  There is no linear growthrate and the system is Toomre stable.");
		
	else
	
		growthrate = sqrt(1/(Q^2)-1);
		kmax = 1/Q;
		lambda = 2*pi/kmax;
		
		#DISPLAY THE RESULTS ON THE SCREEN
		disp(" ");
		disp("Shown are the growthrate (g), the wave vector (k) and the wave length (lambda) of the fastest growing mode.");
		disp(" ");
		#DISPLAY THE GROWTHRATE OF THE FASTEST GROWING MODE
		disp ("The growthrate g is "), disp (growthrate);
		disp(" ");
		#DISPLAY THE WAVE VECTOR OF THE FASTEST GROWING MODE
		disp ("The corresponding wave vector k is "), disp (kmax);
		disp(" ");
		#DISPLAY THE WAVE LENGTH OF THE FASTEST GROWING MODE
		disp ("The wave length lambda is "), disp (lambda);
	
	endif

endif


disp(" ");
disp("The wave length lambda is in units of scale heights:  H = c_{s0}/Omega_0 ");






