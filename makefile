INCLUDES = -I /home/btpp/btp00000/shared/cJSON
CXXFLAGS = -g -Wall -std=c++11 -O3 ${INCLUDES}
OBJS     = disk_flow.o
PROG     = run
LIBS     = -L /home/btpp/btp00000/shared/cJSON/lib -l cjson -lfftw3 -lm



all:            $(PROG)

${PROG}:        $(OBJS)
	$(CXX) $(INCLUDES) -o $(PROG) $(OBJS) $(LIBS)
clean:
	$(RM) -f $(PROG) core *.o
