The code "DiskFlow" provides a program that can be used to simulate turbulent, self-gravitating accretion flows in the local shearing box approximation in the razor-sharp limit. It is a finite difference code on a Eulerian grid. The governing system of equations has been nondimensionalized
and physcially consists of

(1) a continuity equation for the surface mass density, 
(2) the Euler equations (in two dimensions) to determine the flow field based on momentum conservation, 
(3) an equation of state to link the pressure with the internal energy (or vice versa), 
(4) a transport equation for the internal energy density to capture the effects of cooling and shocks,
(5) the poisson equation to to account for self-gravity of the fluid.

Numerically, there are 5 field variables that are included:

(1) the gravitational potential
(2) the surface density
(3) the internal energy density 
(4) the velocity field in radial (x) direction
(5) the velocity field in poloidal (y) direction
 
The grid consists of N_radial * N_poloidal grid points accessed by (i,j). A third index (k) is used to adress a respective quantity (k = 1: gravitational potential, k = 2: surface density, ...).
To avoid unnecessary multidimensional arrays, a single pseudo-multidimensional vector "combining_vector" is used that summarizes all 5 quantities for each grid point, i. e. the 
vector contains 5 * N_radial * N_poloidal entries. Another reason to use a single, pseudo-multidimensional vector is that the fast Fourier transform that solves the poisson equation determining the
gravitational potential from the surface density (using the FFTW3 library) requires those one-dimensional arrays (vectors) as in/output if the memory is allocated dynamically. So, unnecessary and time-consuming
value-transfer can be avoided. Input parameters are changed by a JSON script in a way that one doesn't need to compile the code after each change of a parameter but just have to make a new run.

The derivatives are numerically realised by central differences up to the 8th order, for time-integration it can be chosen between euler scheme or a Runge Kutta scheme of 3rd as well as 4th order. At the
moment, (16.03.2020), only linear interpolation between two grid points is used.
 
Explanations (i. e. comments) to individual program sections will be given in the source-code "disk_flow.cpp" in capital letters before a section begins. On the contrary, comments on individual program lines are made in the same line.

Compile program (example): g++ -std=c++11 -lfftw3 -lm -I /home/btpp/btp00000/shared/cJSON -L /home/btpp/btp00000/shared/cJSON/lib -lcjson disk_flow.cpp -o run
Note that the path provided above might be different for your system. Furthermore, the newest version of "makefile" contains some additional compiler flags that act to increase performance
Run program (if compiled in the way shown above): ./run
Hint: The command must include an instruction for the compiler to link the program with the FFTW3 library (-lfftw3 -lm) as well as the cJSON library

In "input_diskFlow.json" the variable "initialization" must (for a typical start) be set to "6". In case one wants to do a restart, "initialization=16" must be set. Note, that for a restart one must also provide the time 
in orbits (\Omega_0^{-1}) at which the restart is initiated.
